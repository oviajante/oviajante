/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary.service;


import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rita
 */
public class BlogentryFacadeRESTTest {

    private Client client;
    private WebTarget target;

    public BlogentryFacadeRESTTest() {
    }

    @Before
    public void setUp() {
        this.client = ClientBuilder.newClient();
        this.target = client.target("http://deti-tqs-vm6.ua.pt:8080/mavenViajanteTQS-1.0-SNAPSHOT/webresources/entities.blogentry");

    }

    @After
    public void tearDown() {
        client.close();

    }

    @Test
    public void fetchTodos() {
        Response response = target.request(MediaType.APPLICATION_JSON).get();
        assertThat(response.getStatus(), CoreMatchers.is(200));

        JsonArray allTodos = response.readEntity(JsonArray.class);
        System.out.println("Blog Entries: " + allTodos);
        assertFalse(allTodos.isEmpty());

        //ID=100
        JsonObject todoID100 = allTodos.getJsonObject(0);
        assertTrue(todoID100.getString("data").equals("19 Julho, 2013 ás 10:45"));
        assertEquals(todoID100.getInt("id"), 100);

        JsonObject dedicatedTodo = target.path("100").request(MediaType.APPLICATION_JSON).get(JsonObject.class);
        assertTrue(dedicatedTodo.getString("imagemurl").contains("https://www.dropbox.com/s/brvinzql837aju9/uganda.jpg?raw=1"));
        assertTrue(dedicatedTodo.getString("local").contains("uganda"));
        assertTrue(dedicatedTodo.getString("texto").contains("Depois de passar pela capital Kampala e por Jinja, a putativa Meca do rafting em África, por Fort Portal, da qual se diz ser a mais limpa cidade do Uganda."));
        assertTrue(dedicatedTodo.getString("tipoviagem").contains("Viagem Sozinho"));
        assertTrue(dedicatedTodo.getString("titulo").contains("Impressões do Uganda: Lago Bunyonyi"));

        JsonObject t = todoID100.getJsonObject("idUtilizadorId");
        assertTrue(t.getString("email").equals("dm@hotmail.com"));
        assertTrue(t.getString("data").equals("20-04-1984"));
        assertEquals(t.getInt("id"), 104);
        assertTrue(t.getString("nome").equals("Duarte Morais"));
        assertTrue(t.getString("password").equals("dm123"));
        assertEquals(t.getInt("telemovel"),91570402);
        assertTrue(t.getString("utilizador").equals("dm"));

        //ID=102
        JsonObject todoID102 = allTodos.getJsonObject(2);
        assertTrue(todoID102.getString("data").equals("8 junho, 2013 ás 10:45"));
        assertEquals(todoID102.getInt("id"), 102);

        JsonObject dedicatedTodoID102 = target.path("102").request(MediaType.APPLICATION_JSON).get(JsonObject.class);
        assertTrue(dedicatedTodoID102.getString("imagemurl").contains("https://www.dropbox.com/s/k8svrumiaoaqm5m/cape.jpg?raw=1"));
        assertTrue(dedicatedTodoID102.getString("local").contains("cidadecabo"));
        assertTrue(dedicatedTodoID102.getString("texto").contains("Quem acha que férias em África é só sol e calor está muito enganado. Não só não temos apanhado muito calor como ainda levámos com a primeira tempestade do inverno Sul Africano na Cidade do Cabo."));
        assertTrue(dedicatedTodoID102.getString("tipoviagem").contains("Viagem Sozinho"));
        assertTrue(dedicatedTodoID102.getString("titulo").contains("Cidade do Cabo / Cape Town"));

        JsonObject v = todoID102.getJsonObject("idUtilizadorId");
        assertTrue(v.getString("email").equals("da@sapo.pt"));
        assertTrue(v.getString("data").equals("03-06-1980"));
        assertEquals(v.getInt("id"), 106);
        assertTrue(v.getString("nome").equals("Diogo Amaral"));
        assertTrue(v.getString("password").equals("da123"));
        assertEquals(v.getInt("telemovel"),911111001);
        assertTrue(v.getString("utilizador").equals("da"));
        
        //ID=106
        JsonObject todoID106 = allTodos.getJsonObject(6);
        assertTrue(todoID106.getString("data").equals("16 Junho, 2015 ás 09:00"));
        assertEquals(todoID106.getInt("id"), 106);

        JsonObject dedicatedTodoID106 = target.path("106").request(MediaType.APPLICATION_JSON).get(JsonObject.class);
        assertTrue(dedicatedTodoID106.getString("imagemurl").contains("https://www.dropbox.com/s/cykrz1rvg9zz43u/douro.jpg?raw=1"));
        assertTrue(dedicatedTodoID106.getString("local").contains("douro"));
        assertTrue(dedicatedTodoID106.getString("texto").contains("Apesar de ser natural do Porto e atualmente morar em Matosinhos, paredes-meias com a Invicta, nunca tinha feito um cruzeiro no Douro. Da mesma forma que me envergonho de (ainda) não conhecer o Salão Árabe do Palácio da Bolsa"));
        assertTrue(dedicatedTodoID106.getString("tipoviagem").contains("Viagem Sozinho"));
        assertTrue(dedicatedTodoID106.getString("titulo").contains("Apita o comboio num cruzeiro no Douro"));

        JsonObject c = todoID106.getJsonObject("idUtilizadorId");
        assertTrue(c.getString("email").equals("jm@hotmail.com"));
        assertTrue(c.getString("data").equals("23-12-1983"));
        assertEquals(c.getInt("id"), 110);
        assertTrue(c.getString("nome").equals("Joel Marques"));
        assertTrue(c.getString("password").equals("jm123"));
        assertEquals(c.getInt("telemovel"),911111005);
        assertTrue(c.getString("utilizador").equals("jm"));
    }

    /*    
    @Test
    public void testEdit_GenericType() throws Exception {
        System.out.println("edit");
        Blogentry entity = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        BlogentryFacadeREST instance = (BlogentryFacadeREST)container.getContext().lookup("java:global/classes/BlogentryFacadeREST");
        instance.edit(entity);
        container.close();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    
    @Test
    public void testRemove_GenericType() throws Exception {
        System.out.println("remove");
        Blogentry entity = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        BlogentryFacadeREST instance = (BlogentryFacadeREST)container.getContext().lookup("java:global/classes/BlogentryFacadeREST");
        instance.remove(entity);
        container.close();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    
    @Test
    public void testFind_Object() throws Exception {
        System.out.println("find");
        Object id = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        BlogentryFacadeREST instance = (BlogentryFacadeREST)container.getContext().lookup("java:global/classes/BlogentryFacadeREST");
        Blogentry expResult = null;
        Blogentry result = instance.find(id);
        assertEquals(expResult, result);
        container.close();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    
    @Test
    public void testFindRange_intArr() throws Exception {
        System.out.println("findRange");
        int[] range = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        BlogentryFacadeREST instance = (BlogentryFacadeREST)container.getContext().lookup("java:global/classes/BlogentryFacadeREST");
        List<Blogentry> expResult = null;
        List<Blogentry> result = instance.findRange(range);
        assertEquals(expResult, result);
        container.close();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    
    @Test
    public void testCount() throws Exception {
        System.out.println("count");
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        BlogentryFacadeREST instance = (BlogentryFacadeREST)container.getContext().lookup("java:global/classes/BlogentryFacadeREST");
        int expResult = 0;
        int result = instance.count();
        assertEquals(expResult, result);
        container.close();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    
    @Test
    public void testCreate() throws Exception {
        System.out.println("create");
        Blogentry entity = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        BlogentryFacadeREST instance = (BlogentryFacadeREST)container.getContext().lookup("java:global/classes/BlogentryFacadeREST");
        instance.create(entity);
        container.close();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    
    @Test
    public void testEdit_Long_Blogentry() throws Exception {
        System.out.println("edit");
        Long id = null;
        Blogentry entity = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        BlogentryFacadeREST instance = (BlogentryFacadeREST)container.getContext().lookup("java:global/classes/BlogentryFacadeREST");
        instance.edit(id, entity);
        container.close();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    
    @Test
    public void testRemove_Long() throws Exception {
        System.out.println("remove");
        Long id = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        BlogentryFacadeREST instance = (BlogentryFacadeREST)container.getContext().lookup("java:global/classes/BlogentryFacadeREST");
        instance.remove(id);
        container.close();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    
    @Test
    public void testFind_Long() throws Exception {
        System.out.println("find");
        Long id = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        BlogentryFacadeREST instance = (BlogentryFacadeREST)container.getContext().lookup("java:global/classes/BlogentryFacadeREST");
        Blogentry expResult = null;
        Blogentry result = instance.find(id);
        assertEquals(expResult, result);
        container.close();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    @Test
    public void testFindAll() throws Exception {
        System.out.println("findAll");
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        BlogentryFacadeREST instance = (BlogentryFacadeREST)container.getContext().lookup("java:global/classes/BlogentryFacadeREST");
        List<Blogentry> expResult = null;
        List<Blogentry> result = instance.findAll();
        assertEquals(expResult, result);
        container.close();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    
    @Test
    public void testFindRange_Integer_Integer() throws Exception {
        System.out.println("findRange");
        Integer from = null;
        Integer to = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        BlogentryFacadeREST instance = (BlogentryFacadeREST)container.getContext().lookup("java:global/classes/BlogentryFacadeREST");
        List<Blogentry> expResult = null;
        List<Blogentry> result = instance.findRange(from, to);
        assertEquals(expResult, result);
        container.close();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    
    @Test
    public void testCountREST() throws Exception {
        System.out.println("countREST");
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        BlogentryFacadeREST instance = (BlogentryFacadeREST)container.getContext().lookup("java:global/classes/BlogentryFacadeREST");
        String expResult = "";
        String result = instance.countREST();
        assertEquals(expResult, result);
        container.close();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }*/
}
