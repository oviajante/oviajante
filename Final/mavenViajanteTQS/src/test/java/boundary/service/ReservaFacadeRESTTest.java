/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary.service;

import entities.Reserva;
import java.util.List;
import javax.ejb.embeddable.EJBContainer;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sarafurao
 */
public class ReservaFacadeRESTTest {
    private Client client;
    private WebTarget target;
    
    
    public ReservaFacadeRESTTest() {
    }
    
    
    @Before
    public void initReserva() {
        this.client = ClientBuilder.newClient();
        this.target = client.target("http://deti-tqs-vm6.ua.pt:8080/mavenViajanteTQS-1.0-SNAPSHOT/webresources/entities.reserva");

    }
    
    @Test
    public void fetchTodos() {
        Response response = target.request(MediaType.APPLICATION_JSON).get();
        assertThat(response.getStatus(), CoreMatchers.is(200));

        JsonArray allTodos = response.readEntity(JsonArray.class);
        System.out.println("Todas as reservas " + allTodos);
        assertFalse(allTodos.isEmpty());

        
        // GET reserva
        JsonObject jsonpacoteid114teste= allTodos.getJsonObject(0);
        assertTrue(jsonpacoteid114teste.getString("datapartida").equals("17-06-2013"));
        assertEquals(jsonpacoteid114teste.getInt("id"),114);
        JsonObject c = jsonpacoteid114teste.getJsonObject("idPacoteviagemId");
        assertTrue(c.getString("alojamento").equals("Miami Beach Resort & Spa"));
        assertTrue(c.getString("condicoes").equals("Condições/Serviços Incluídos: \n" +
"- Passagem aérea em classe U ou na nossa classe de allotment com a TAP PORTUGALCondições/Serviços Excluidos: \n" +
"-Eletronic System for Travel Authorization"));
        assertTrue(c.getString("detalhe").equals("americaNorte"));
        assertEquals(c.getInt("id"),128);
        
        JsonObject d = c.getJsonObject("idAgenciaId");
        assertTrue(d.getString("companhia").equals("Agencia Tap"));
        assertTrue(d.getString("password").equals("tap123"));
        assertTrue(d.getString("utilizador").equals("tap"));
        assertEquals(d.getInt("id"),101);
        
        assertTrue(c.getString("imagemurl").equals("https://www.dropbox.com/s/vjmrwu6v7jfkwij/americadonorte2.jpg?raw=1"));
        assertTrue(c.getString("itenerario").equals("Dia  1 Faro / MiamiDia 2 a 7 MiamiDia  8 Miami / Lisboa, Porto ou FaroDia  9 Faro"));
        assertTrue(c.getString("preco").equals("1.335€"));
        assertTrue(c.getString("tipopacote").equals("grandesViagens"));
        assertTrue(c.getString("titulo").equals("EUA - MIAMI É VICIANTE - NÃO FIQUE EM CASA"));
     
        
        
        JsonObject jsonpacoteid115teste = allTodos.getJsonObject(1);
        assertTrue(jsonpacoteid115teste.getString("datapartida").equals("27-08-2016"));
        assertEquals(jsonpacoteid115teste.getInt("id"),115);
        JsonObject a = jsonpacoteid115teste.getJsonObject("idPacoteviagemId");
        assertTrue(a.getString("alojamento").equals("Não incluído."));
        assertTrue(a.getString("detalhe").equals("espanha"));
        assertEquals(a.getInt("id"),118);
        
        JsonObject b = a.getJsonObject("idAgenciaId");
        assertTrue(b.getString("companhia").equals("Agencia Tap"));
        assertTrue(b.getString("password").equals("tap123"));
        assertTrue(b.getString("utilizador").equals("tap"));
        assertEquals(b.getInt("id"),101);
        
        assertTrue(a.getString("imagemurl").equals("https://www.dropbox.com/s/93ad4pslg5y1d4p/espanha.jpg?raw=1"));
        assertTrue(a.getString("itenerario").equals("Na viagem...Ciudade Jardin, Can Pastilla e El Arenal são alguns dos locais mais turísticos de Palma de Maiorca."));
        assertTrue(a.getString("preco").equals("346€"));
        assertTrue(a.getString("tipopacote").equals("praias"));
        assertTrue(a.getString("titulo").equals("MAIORCA - CIDADE JARDIN, CAN PASTILLA"));

        
        // GET reserva with id
        JsonObject dedicatedTodoreserva114teste= target.path("114").request(MediaType.APPLICATION_JSON).get(JsonObject.class);
        System.out.println("Get reserva with id "+dedicatedTodoreserva114teste);
        assertTrue(dedicatedTodoreserva114teste.getString("datapartida").contains("17-06-2013")); 
       

  
        JsonObject dedicatedTodoreserva115teste = target.path("115").request(MediaType.APPLICATION_JSON).get(JsonObject.class);
        System.out.println("Get reserva with id "+dedicatedTodoreserva115teste);
        assertTrue(dedicatedTodoreserva115teste.getString("datapartida").contains("27-08-2016")); 
 
     
     /*
        JsonObject jsonObject = (JsonObject) Json.createObjectBuilder()
            .add("companhia","Top Atlantico")
            .add("id","4")
            .add("password","top123")
            .add("utilizador","topatlantico")
            .build();
        
     */   

               
    }

 
    public void deleteTodos() {
        Response response = target.path("100").request().delete();
        assertThat(response.getStatus(), CoreMatchers.is(200));
    }

    @After
    public void cleanup() {
        client.close();
    }
   
    /*
    @Test
    public void testEdit_GenericType() throws Exception {
        System.out.println("edit");
        Reserva entity = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        ReservaFacadeREST instance = (ReservaFacadeREST)container.getContext().lookup("java:global/classes/ReservaFacadeREST");
        instance.edit(entity);
        container.close();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

   
    @Test
    public void testRemove_GenericType() throws Exception {
        System.out.println("remove");
        Reserva entity = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        ReservaFacadeREST instance = (ReservaFacadeREST)container.getContext().lookup("java:global/classes/ReservaFacadeREST");
        instance.remove(entity);
        container.close();

    }

    
    @Test
    public void testFind_Object() throws Exception {
        System.out.println("find");
        Object id = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        ReservaFacadeREST instance = (ReservaFacadeREST)container.getContext().lookup("java:global/classes/ReservaFacadeREST");
        Reserva expResult = null;
        Reserva result = instance.find(id);
        assertEquals(expResult, result);
        container.close();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

 
    @Test
    public void testFindRange_intArr() throws Exception {
        System.out.println("findRange");
        int[] range = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        ReservaFacadeREST instance = (ReservaFacadeREST)container.getContext().lookup("java:global/classes/ReservaFacadeREST");
        List<Reserva> expResult = null;
        List<Reserva> result = instance.findRange(range);
        assertEquals(expResult, result);
        container.close();
     
    }

   
    @Test
    public void testCount() throws Exception {
        System.out.println("count");
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        ReservaFacadeREST instance = (ReservaFacadeREST)container.getContext().lookup("java:global/classes/ReservaFacadeREST");
        int expResult = 0;
        int result = instance.count();
        assertEquals(expResult, result);
        container.close();
   
    }

  
    @Test
    public void testCreate() throws Exception {
        System.out.println("create");
        Reserva entity = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        ReservaFacadeREST instance = (ReservaFacadeREST)container.getContext().lookup("java:global/classes/ReservaFacadeREST");
        instance.create(entity);
        container.close();
      
    }

  
    @Test
    public void testEdit_Long_Reserva() throws Exception {
        System.out.println("edit");
        Long id = null;
        Reserva entity = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        ReservaFacadeREST instance = (ReservaFacadeREST)container.getContext().lookup("java:global/classes/ReservaFacadeREST");
        instance.edit(id, entity);
        container.close();
      
    }

   
    @Test
    public void testRemove_Long() throws Exception {
        System.out.println("remove");
        Long id = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        ReservaFacadeREST instance = (ReservaFacadeREST)container.getContext().lookup("java:global/classes/ReservaFacadeREST");
        instance.remove(id);
        container.close();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

 
    @Test
    public void testFind_Long() throws Exception {
        System.out.println("find");
        Long id = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        ReservaFacadeREST instance = (ReservaFacadeREST)container.getContext().lookup("java:global/classes/ReservaFacadeREST");
        Reserva expResult = null;
        Reserva result = instance.find(id);
        assertEquals(expResult, result);
        container.close();
     
    }


    @Test
    public void testFindAll() throws Exception {
        System.out.println("findAll");
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        ReservaFacadeREST instance = (ReservaFacadeREST)container.getContext().lookup("java:global/classes/ReservaFacadeREST");
        List<Reserva> expResult = null;
        List<Reserva> result = instance.findAll();
        assertEquals(expResult, result);
        container.close();
    
    }


    @Test
    public void testFindRange_Integer_Integer() throws Exception {
        System.out.println("findRange");
        Integer from = null;
        Integer to = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        ReservaFacadeREST instance = (ReservaFacadeREST)container.getContext().lookup("java:global/classes/ReservaFacadeREST");
        List<Reserva> expResult = null;
        List<Reserva> result = instance.findRange(from, to);
        assertEquals(expResult, result);
        container.close();
      
    }

 
    @Test
    public void testCountREST() throws Exception {
        System.out.println("countREST");
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        ReservaFacadeREST instance = (ReservaFacadeREST)container.getContext().lookup("java:global/classes/ReservaFacadeREST");
        String expResult = "";
        String result = instance.countREST();
        assertEquals(expResult, result);
        container.close();
     
    }

*/
    
}
