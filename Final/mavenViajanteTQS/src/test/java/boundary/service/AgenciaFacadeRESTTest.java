/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary.service;

import entities.Agencia;
import java.util.List;
import javax.ejb.embeddable.EJBContainer;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sarafurao
 */
public class AgenciaFacadeRESTTest {
    private Client client;
    private WebTarget target;
    
    
    public AgenciaFacadeRESTTest() {
    }
    
   
    
     
    @Before
    public void initAgencia() {
        this.client = ClientBuilder.newClient();
        this.target = client.target("http://deti-tqs-vm6.ua.pt:8080/mavenViajanteTQS-1.0-SNAPSHOT/webresources/entities.agencia");

    }
    
    @Test
    public void fetchTodos() {
        Response response = target.request(MediaType.APPLICATION_JSON).get();
        assertThat(response.getStatus(), CoreMatchers.is(200));

        JsonArray allTodos = response.readEntity(JsonArray.class);
        System.out.println("Todas as agencias " + allTodos);
        assertFalse(allTodos.isEmpty());

        
        // GET 
        JsonObject jsonagenciateste = allTodos.getJsonObject(0);
        assertTrue(jsonagenciateste.getString("companhia").equals("Agencia de teste"));
        assertTrue(jsonagenciateste.getString("password").equals("123"));
        assertTrue(jsonagenciateste.getString("utilizador").equals("agencia"));
        
        
        JsonObject jsonagenciahalcon= allTodos.getJsonObject(1);
        assertTrue(jsonagenciahalcon.getString("companhia").equals("Agencia Halcon"));
        assertTrue(jsonagenciahalcon.getString("password").equals("halcon123"));
        assertTrue(jsonagenciahalcon.getString("utilizador").equals("halcon"));
        
        JsonObject jsonagenciabreu= allTodos.getJsonObject(2);
        assertTrue(jsonagenciabreu.getString("companhia").equals("Agencia Abreu"));
        assertTrue(jsonagenciabreu.getString("password").equals("abreu123"));
        assertTrue(jsonagenciabreu.getString("utilizador").equals("abreu"));
        
        JsonObject jsonagenciatap= allTodos.getJsonObject(3);
        assertTrue(jsonagenciatap.getString("companhia").equals("Agencia Tap"));
        assertTrue(jsonagenciatap.getString("password").equals("tap123"));
        assertTrue(jsonagenciatap.getString("utilizador").equals("tap"));
      
     
        
        
        // GET agencia with id
        JsonObject dedicatedTodoagenciateste= target.path("1").request(MediaType.APPLICATION_JSON).get(JsonObject.class);
        System.out.println("Get Agencia de Teste with id "+dedicatedTodoagenciateste);
        assertTrue(dedicatedTodoagenciateste.getString("companhia").contains("Agencia de teste")); 

        JsonObject dedicatedTodoagenciaabreu = target.path("100").request(MediaType.APPLICATION_JSON).get(JsonObject.class);
        System.out.println("Get Agencia Abreu with id "+dedicatedTodoagenciaabreu);
        assertTrue(dedicatedTodoagenciaabreu.getString("companhia").contains("Agencia Abreu")); 

        JsonObject dedicatedTodoagenciahalcon = target.path("3").request(MediaType.APPLICATION_JSON).get(JsonObject.class);
        System.out.println("Get Agencia Halcon with id "+dedicatedTodoagenciahalcon);
        assertTrue(dedicatedTodoagenciahalcon.getString("companhia").contains("Agencia Halcon")); 
        
        JsonObject dedicatedTodoagenciatap = target.path("101").request(MediaType.APPLICATION_JSON).get(JsonObject.class);
        System.out.println("Get Agencia Tap with id "+dedicatedTodoagenciatap);
        assertTrue(dedicatedTodoagenciatap.getString("companhia").contains("Agencia Tap")); 

     
     /*
        JsonObject jsonObject = (JsonObject) Json.createObjectBuilder()
            .add("companhia","Top Atlantico")
            .add("id","4")
            .add("password","top123")
            .add("utilizador","topatlantico")
            .build();
        
     */   

               
    }

 
    public void deleteTodos() {
        Response response = target.path("100").request().delete();
        assertThat(response.getStatus(), CoreMatchers.is(200));
    }

    @After
    public void cleanup() {
        client.close();
    }

    /*
    @Test
    public void testEdit_GenericType() throws Exception {
        System.out.println("edit");
        Agencia entity = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        AgenciaFacadeREST instance = (AgenciaFacadeREST)container.getContext().lookup("java:global/classes/AgenciaFacadeREST");
        instance.edit(entity);
        container.close();
    
    }

  
    @Test
    public void testRemove_GenericType() throws Exception {
        System.out.println("remove");
        Agencia entity = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        AgenciaFacadeREST instance = (AgenciaFacadeREST)container.getContext().lookup("java:global/classes/AgenciaFacadeREST");
        instance.remove(entity);
        container.close();
      
    }

 
    @Test
    public void testFind_Object() throws Exception {
        System.out.println("find");
        Object id = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        AgenciaFacadeREST instance = (AgenciaFacadeREST)container.getContext().lookup("java:global/classes/AgenciaFacadeREST");
        Agencia expResult = null;
        Agencia result = instance.find(id);
        assertEquals(expResult, result);
        container.close();
    
    }


    @Test
    public void testFindRange_intArr() throws Exception {
        System.out.println("findRange");
        int[] range = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        AgenciaFacadeREST instance = (AgenciaFacadeREST)container.getContext().lookup("java:global/classes/AgenciaFacadeREST");
        List<Agencia> expResult = null;
        List<Agencia> result = instance.findRange(range);
        assertEquals(expResult, result);
        container.close();
      
    }

  
    @Test
    public void testCount() throws Exception {
        System.out.println("count");
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        AgenciaFacadeREST instance = (AgenciaFacadeREST)container.getContext().lookup("java:global/classes/AgenciaFacadeREST");
        int expResult = 0;
        int result = instance.count();
        assertEquals(expResult, result);
        container.close();
     
    }


    @Test
    public void testCreate() throws Exception {
        System.out.println("create");
        Agencia entity = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        AgenciaFacadeREST instance = (AgenciaFacadeREST)container.getContext().lookup("java:global/classes/AgenciaFacadeREST");
        instance.create(entity);
        container.close();
    
    }

    
    @Test
    public void testEdit_Long_Agencia() throws Exception {
        System.out.println("edit");
        Long id = null;
        Agencia entity = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        AgenciaFacadeREST instance = (AgenciaFacadeREST)container.getContext().lookup("java:global/classes/AgenciaFacadeREST");
        instance.edit(id, entity);
        container.close();
      
    }

    @Test
    public void testRemove_Long() throws Exception {
        System.out.println("remove");
        Long id = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        AgenciaFacadeREST instance = (AgenciaFacadeREST)container.getContext().lookup("java:global/classes/AgenciaFacadeREST");
        instance.remove(id);
        container.close();

    }


    @Test
    public void testFind_Long() throws Exception {
        System.out.println("find");
        Long id = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        AgenciaFacadeREST instance = (AgenciaFacadeREST)container.getContext().lookup("java:global/classes/AgenciaFacadeREST");
        Agencia expResult = null;
        Agencia result = instance.find(id);
        assertEquals(expResult, result);
        container.close();
     
    }

    @Test
    public void testFindAll() throws Exception {
        System.out.println("findAll");
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        AgenciaFacadeREST instance = (AgenciaFacadeREST)container.getContext().lookup("java:global/classes/AgenciaFacadeREST");
        List<Agencia> expResult = null;
        List<Agencia> result = instance.findAll();
        assertEquals(expResult, result);
        container.close();
       
    }

    @Test
    public void testFindRange_Integer_Integer() throws Exception {
        System.out.println("findRange");
        Integer from = null;
        Integer to = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        AgenciaFacadeREST instance = (AgenciaFacadeREST)container.getContext().lookup("java:global/classes/AgenciaFacadeREST");
        List<Agencia> expResult = null;
        List<Agencia> result = instance.findRange(from, to);
        assertEquals(expResult, result);
        container.close();
       
    }


    @Test
    public void testCountREST() throws Exception {
        System.out.println("countREST");
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        AgenciaFacadeREST instance = (AgenciaFacadeREST)container.getContext().lookup("java:global/classes/AgenciaFacadeREST");
        String expResult = "";
        String result = instance.countREST();
        assertEquals(expResult, result);
        container.close();

    }

*/
    
}
