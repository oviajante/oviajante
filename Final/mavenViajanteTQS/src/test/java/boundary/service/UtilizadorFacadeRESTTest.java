/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary.service;

import entities.Utilizador;
import java.util.List;
import javax.ejb.embeddable.EJBContainer;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sarafurao
 */
public class UtilizadorFacadeRESTTest {
    private Client client;
    private WebTarget target;
    
    public UtilizadorFacadeRESTTest() {
    }
    
    @Before
    public void initReserva() {
        this.client = ClientBuilder.newClient();
        this.target = client.target("http://deti-tqs-vm6.ua.pt:8080/mavenViajanteTQS-1.0-SNAPSHOT/webresources/entities.utilizador");

    }
    
    @Test
    public void fetchTodos() {
        Response response = target.request(MediaType.APPLICATION_JSON).get();
        assertThat(response.getStatus(), CoreMatchers.is(200));

        JsonArray allTodos = response.readEntity(JsonArray.class);
        System.out.println("Todos os utilizadores " + allTodos);
        assertFalse(allTodos.isEmpty());

        
        // GET utilizador
        JsonObject jsonutilizador1 = allTodos.getJsonObject(0);
        assertTrue(jsonutilizador1.getString("data").equals("06-05-1994"));
        assertTrue(jsonutilizador1.getString("email").equals("joaoesteves@ua.pt"));
        assertTrue(jsonutilizador1.getString("nome").equals("Joao Esteves"));
        assertTrue(jsonutilizador1.getString("password").equals("joaoesteves123"));
        assertTrue(jsonutilizador1.getString("utilizador").equals("joaoesteves"));
        assertEquals(jsonutilizador1.getInt("telemovel"),910251788);
 
        JsonObject jsonutilizador105= allTodos.getJsonObject(7);
        assertTrue(jsonutilizador105.getString("data").equals("28-02-1990"));
        assertTrue(jsonutilizador105.getString("email").equals("dc@hotmail.com"));
        assertTrue(jsonutilizador105.getString("nome").equals("Diogo Coelho"));
        assertTrue(jsonutilizador105.getString("password").equals("dc123"));
        assertTrue(jsonutilizador105.getString("utilizador").equals("dc"));
        assertEquals(jsonutilizador105.getInt("telemovel"),911111000);
        
        
        // GET utilizador with id
        JsonObject dedicatedTodojsonutilizador1teste= target.path("1").request(MediaType.APPLICATION_JSON).get(JsonObject.class);
        System.out.println("Get utilizador with id "+dedicatedTodojsonutilizador1teste);
        assertTrue(dedicatedTodojsonutilizador1teste.getString("nome").contains("Joao Esteves")); 
  
        JsonObject dedicatedTodojsonutilizador51teste = target.path("51").request(MediaType.APPLICATION_JSON).get(JsonObject.class);
        System.out.println("Get utilizador with id "+dedicatedTodojsonutilizador51teste);
        assertTrue(dedicatedTodojsonutilizador51teste.getString("nome").contains("Joao Esteves secundario")); 

               
    }

    
    public void deleteTodos() {
        Response response = target.path("100").request().delete();
        assertThat(response.getStatus(), CoreMatchers.is(200));
    }

    @After
    public void cleanup() {
        client.close();
    }


    /*
    @Test
    public void testEdit_GenericType() throws Exception {
        System.out.println("edit");
        Utilizador entity = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        UtilizadorFacadeREST instance = (UtilizadorFacadeREST)container.getContext().lookup("java:global/classes/UtilizadorFacadeREST");
        instance.edit(entity);
        container.close();
      
    }

   
    @Test
    public void testRemove_GenericType() throws Exception {
        System.out.println("remove");
        Utilizador entity = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        UtilizadorFacadeREST instance = (UtilizadorFacadeREST)container.getContext().lookup("java:global/classes/UtilizadorFacadeREST");
        instance.remove(entity);
        container.close();
   
    }

  
    @Test
    public void testFind_Object() throws Exception {
        System.out.println("find");
        Object id = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        UtilizadorFacadeREST instance = (UtilizadorFacadeREST)container.getContext().lookup("java:global/classes/UtilizadorFacadeREST");
        Utilizador expResult = null;
        Utilizador result = instance.find(id);
        assertEquals(expResult, result);
        container.close();
   
    }

    
    @Test
    public void testFindRange_intArr() throws Exception {
        System.out.println("findRange");
        int[] range = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        UtilizadorFacadeREST instance = (UtilizadorFacadeREST)container.getContext().lookup("java:global/classes/UtilizadorFacadeREST");
        List<Utilizador> expResult = null;
        List<Utilizador> result = instance.findRange(range);
        assertEquals(expResult, result);
        container.close();
      
    }

    @Test
    public void testCount() throws Exception {
        System.out.println("count");
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        UtilizadorFacadeREST instance = (UtilizadorFacadeREST)container.getContext().lookup("java:global/classes/UtilizadorFacadeREST");
        int expResult = 0;
        int result = instance.count();
        assertEquals(expResult, result);
        container.close();
      
    }

 
    @Test
    public void testCreate() throws Exception {
        System.out.println("create");
        Utilizador entity = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        UtilizadorFacadeREST instance = (UtilizadorFacadeREST)container.getContext().lookup("java:global/classes/UtilizadorFacadeREST");
        instance.create(entity);
        container.close();
      
    }

   
    @Test
    public void testEdit_Long_Utilizador() throws Exception {
        System.out.println("edit");
        Long id = null;
        Utilizador entity = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        UtilizadorFacadeREST instance = (UtilizadorFacadeREST)container.getContext().lookup("java:global/classes/UtilizadorFacadeREST");
        instance.edit(id, entity);
        container.close();
     
    }

    
    @Test
    public void testRemove_Long() throws Exception {
        System.out.println("remove");
        Long id = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        UtilizadorFacadeREST instance = (UtilizadorFacadeREST)container.getContext().lookup("java:global/classes/UtilizadorFacadeREST");
        instance.remove(id);
        container.close();
      
    }

    
    @Test
    public void testFind_Long() throws Exception {
        System.out.println("find");
        Long id = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        UtilizadorFacadeREST instance = (UtilizadorFacadeREST)container.getContext().lookup("java:global/classes/UtilizadorFacadeREST");
        Utilizador expResult = null;
        Utilizador result = instance.find(id);
        assertEquals(expResult, result);
        container.close();
     
    }

   
    @Test
    public void testFindAll() throws Exception {
        System.out.println("findAll");
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        UtilizadorFacadeREST instance = (UtilizadorFacadeREST)container.getContext().lookup("java:global/classes/UtilizadorFacadeREST");
        List<Utilizador> expResult = null;
        List<Utilizador> result = instance.findAll();
        assertEquals(expResult, result);
        container.close();
        
    }

  
    @Test
    public void testFindRange_Integer_Integer() throws Exception {
        System.out.println("findRange");
        Integer from = null;
        Integer to = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        UtilizadorFacadeREST instance = (UtilizadorFacadeREST)container.getContext().lookup("java:global/classes/UtilizadorFacadeREST");
        List<Utilizador> expResult = null;
        List<Utilizador> result = instance.findRange(from, to);
        assertEquals(expResult, result);
        container.close();
      
    }


    @Test
    public void testCountREST() throws Exception {
        System.out.println("countREST");
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        UtilizadorFacadeREST instance = (UtilizadorFacadeREST)container.getContext().lookup("java:global/classes/UtilizadorFacadeREST");
        String expResult = "";
        String result = instance.countREST();
        assertEquals(expResult, result);
        container.close();
    
    }
    */
}
