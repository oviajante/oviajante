/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary.service;

import entities.Pacoteviagem;
import java.util.List;
import javax.ejb.embeddable.EJBContainer;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rita
 */
public class PacoteviagemFacadeRESTTest {

    private Client client;
    private WebTarget target;

    public PacoteviagemFacadeRESTTest() {
    }

    @Before
    public void setUp() {
        this.client = ClientBuilder.newClient();
        this.target = client.target("http://deti-tqs-vm6.ua.pt:8080/mavenViajanteTQS-1.0-SNAPSHOT/webresources/entities.pacoteviagem");

    }

    @After
    public void tearDown() {
        client.close();
    }

    @Test
    public void fetchTodos() {
        Response response = target.request(MediaType.APPLICATION_JSON).get();
        assertThat(response.getStatus(), CoreMatchers.is(200));

        JsonArray allTodos = response.readEntity(JsonArray.class);
        System.out.println("Pacote Viagem: " + allTodos);
        assertFalse(allTodos.isEmpty());

        //ID=100
        JsonObject todoID100 = allTodos.getJsonObject(0);
        assertTrue(todoID100.getString("condicoes").equals("Condições/Serviços Incluídos: - A consultar"));
        assertEquals(todoID100.getInt("id"), 100);
        assertTrue(todoID100.getString("detalhe").equals("mauricias"));
        assertTrue(todoID100.getString("imagemurl").equals("https://www.dropbox.com/s/vhmozq3io8lof9i/mauricias.jpg?raw=1"));
        assertTrue(todoID100.getString("preco").equals("1.379€"));
        assertTrue(todoID100.getString("tipopacote").equals("promocoes"));
        assertTrue(todoID100.getString("titulo").equals("LUXURIANTE MAURICIA - LUGARES GARANTIDOS COM A TURKISH AIRLINES"));
        assertTrue(todoID100.getString("alojamento").equals("alojamento: 7 noites de estadia no hotel e regime escolhido + transfers in/out | 10  dia(s)"));

        JsonObject t = todoID100.getJsonObject("idAgenciaId");
        assertTrue(t.getString("companhia").equals("Agencia Abreu"));
        assertEquals(t.getInt("id"), 100);
        assertTrue(t.getString("password").equals("abreu123"));
        assertTrue(t.getString("utilizador").equals("abreu"));

        //ID=109
        JsonObject todoID109 = allTodos.getJsonObject(9);
        assertTrue(todoID109.getString("condicoes").equals("Janela*, Sala de Estar**, TV LCD-HD interactiva de 23 polegadas, Duas camas baixas convertíveis num queen (execepto quando indicado)"));
        assertEquals(todoID109.getInt("id"), 109);
        assertTrue(todoID109.getString("detalhe").equals("veneza"));
        assertTrue(todoID109.getString("imagemurl").equals("https://www.dropbox.com/s/b65forr8u04p1zd/veneza.jpg?raw=1"));
        assertTrue(todoID109.getString("preco").equals("706€"));
        assertTrue(todoID109.getString("tipopacote").equals("cruzeiros"));
        assertTrue(todoID109.getString("titulo").equals("Itália e Croátia (Mediterraneo e Adriático)"));
        assertTrue(todoID109.getString("alojamento").equals("Cabine interior"));

        JsonObject v = todoID109.getJsonObject("idAgenciaId");
        assertTrue(v.getString("companhia").equals("Agencia Abreu"));
        assertEquals(v.getInt("id"), 100);
        assertTrue(v.getString("password").equals("abreu123"));
        assertTrue(v.getString("utilizador").equals("abreu"));

        //ID=116
        JsonObject todoID116 = allTodos.getJsonObject(16);
        assertTrue(todoID116.getString("condicoes").equals("Condições/Serviços Incluídos: \n"
                + "- Passagem aérea em classe turística;- Visto Turístico;Condições/Serviços Excluidos: \n"
                + "- Taxa turística de eur 2 pessoa e noite, aplicável a maiores de 15 anos, paga localmente no hotel;"));
        assertEquals(todoID116.getInt("id"), 116);
        assertTrue(todoID116.getString("detalhe").equals("caboVerde"));
        assertTrue(todoID116.getString("imagemurl").equals("https://www.dropbox.com/s/txyscj91rrzm5bh/caboverde.jpg?raw=1"));
        assertTrue(todoID116.getString("preco").equals("598€"));
        assertTrue(todoID116.getString("tipopacote").equals("praias"));
        assertTrue(todoID116.getString("titulo").equals("ILHA DO SAL - ESPECIAL FÉRIAS DE VERÃO!"));
        assertTrue(todoID116.getString("alojamento").equals("Em primeira linha na praia de Santa Maria, mesmo ao lado do Oásis Novorizonte. A 150 metros do centro da vila."));

        JsonObject c = todoID116.getJsonObject("idAgenciaId");
        assertTrue(c.getString("companhia").equals("Agencia Tap"));
        assertEquals(c.getInt("id"), 101);
        assertTrue(c.getString("password").equals("tap123"));
        assertTrue(c.getString("utilizador").equals("tap"));

    }

    /*
    @Test
    public void testEdit_GenericType() throws Exception {
        System.out.println("edit");
        Pacoteviagem entity = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        PacoteviagemFacadeREST instance = (PacoteviagemFacadeREST)container.getContext().lookup("java:global/classes/PacoteviagemFacadeREST");
        instance.edit(entity);
        container.close();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    
    public void testRemove_GenericType() throws Exception {
        System.out.println("remove");
        Pacoteviagem entity = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        PacoteviagemFacadeREST instance = (PacoteviagemFacadeREST)container.getContext().lookup("java:global/classes/PacoteviagemFacadeREST");
        instance.remove(entity);
        container.close();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    
    @Test
    public void testFind_Object() throws Exception {
        System.out.println("find");
        Object id = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        PacoteviagemFacadeREST instance = (PacoteviagemFacadeREST)container.getContext().lookup("java:global/classes/PacoteviagemFacadeREST");
        Pacoteviagem expResult = null;
        Pacoteviagem result = instance.find(id);
        assertEquals(expResult, result);
        container.close();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    
    @Test
    public void testFindRange_intArr() throws Exception {
        System.out.println("findRange");
        int[] range = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        PacoteviagemFacadeREST instance = (PacoteviagemFacadeREST)container.getContext().lookup("java:global/classes/PacoteviagemFacadeREST");
        List<Pacoteviagem> expResult = null;
        List<Pacoteviagem> result = instance.findRange(range);
        assertEquals(expResult, result);
        container.close();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    
    @Test
    public void testCount() throws Exception {
        System.out.println("count");
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        PacoteviagemFacadeREST instance = (PacoteviagemFacadeREST)container.getContext().lookup("java:global/classes/PacoteviagemFacadeREST");
        int expResult = 0;
        int result = instance.count();
        assertEquals(expResult, result);
        container.close();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    
    @Test
    public void testCreate() throws Exception {
        System.out.println("create");
        Pacoteviagem entity = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        PacoteviagemFacadeREST instance = (PacoteviagemFacadeREST)container.getContext().lookup("java:global/classes/PacoteviagemFacadeREST");
        instance.create(entity);
        container.close();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    
    @Test
    public void testEdit_Long_Pacoteviagem() throws Exception {
        System.out.println("edit");
        Long id = null;
        Pacoteviagem entity = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        PacoteviagemFacadeREST instance = (PacoteviagemFacadeREST)container.getContext().lookup("java:global/classes/PacoteviagemFacadeREST");
        instance.edit(id, entity);
        container.close();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    
    @Test
    public void testRemove_Long() throws Exception {
        System.out.println("remove");
        Long id = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        PacoteviagemFacadeREST instance = (PacoteviagemFacadeREST)container.getContext().lookup("java:global/classes/PacoteviagemFacadeREST");
        instance.remove(id);
        container.close();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    
    @Test
    public void testFind_Long() throws Exception {
        System.out.println("find");
        Long id = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        PacoteviagemFacadeREST instance = (PacoteviagemFacadeREST)container.getContext().lookup("java:global/classes/PacoteviagemFacadeREST");
        Pacoteviagem expResult = null;
        Pacoteviagem result = instance.find(id);
        assertEquals(expResult, result);
        container.close();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    @Test
    public void testFindAll() throws Exception {
        System.out.println("findAll");
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        PacoteviagemFacadeREST instance = (PacoteviagemFacadeREST)container.getContext().lookup("java:global/classes/PacoteviagemFacadeREST");
        List<Pacoteviagem> expResult = null;
        List<Pacoteviagem> result = instance.findAll();
        assertEquals(expResult, result);
        container.close();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    
    @Test
    public void testFindRange_Integer_Integer() throws Exception {
        System.out.println("findRange");
        Integer from = null;
        Integer to = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        PacoteviagemFacadeREST instance = (PacoteviagemFacadeREST)container.getContext().lookup("java:global/classes/PacoteviagemFacadeREST");
        List<Pacoteviagem> expResult = null;
        List<Pacoteviagem> result = instance.findRange(from, to);
        assertEquals(expResult, result);
        container.close();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    
    @Test
    public void testCountREST() throws Exception {
        System.out.println("countREST");
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        PacoteviagemFacadeREST instance = (PacoteviagemFacadeREST)container.getContext().lookup("java:global/classes/PacoteviagemFacadeREST");
        String expResult = "";
        String result = instance.countREST();
        assertEquals(expResult, result);
        container.close();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }*/
}
