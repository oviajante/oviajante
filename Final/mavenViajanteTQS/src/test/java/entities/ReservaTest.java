/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rita
 */
public class ReservaTest {
    
    public ReservaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getId method, of class Reserva.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");
        Reserva instance = new Reserva();
        Long expResult = null;
        Long result = instance.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setId method, of class Reserva.
     */
    @Test
    public void testSetId() {
        System.out.println("setId");
        Long id = null;
        Reserva instance = new Reserva();
        instance.setId(id);
    }

    /**
     * Test of getDatapartida method, of class Reserva.
     */
    @Test
    public void testGetDatapartida() {
        System.out.println("getDatapartida");
        Reserva instance = new Reserva();
        String expResult = null;
        String result = instance.getDatapartida();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDatapartida method, of class Reserva.
     */
    @Test
    public void testSetDatapartida() {
        System.out.println("setDatapartida");
        String datapartida = "";
        Reserva instance = new Reserva();
        instance.setDatapartida(datapartida);
    }

    /**
     * Test of getIdPacoteviagemId method, of class Reserva.
     */
    @Test
    public void testGetIdPacoteviagemId() {
        System.out.println("getIdPacoteviagemId");
        Reserva instance = new Reserva();
        Pacoteviagem expResult = null;
        Pacoteviagem result = instance.getIdPacoteviagemId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setIdPacoteviagemId method, of class Reserva.
     */
    @Test
    public void testSetIdPacoteviagemId() {
        System.out.println("setIdPacoteviagemId");
        Pacoteviagem idPacoteviagemId = null;
        Reserva instance = new Reserva();
        instance.setIdPacoteviagemId(idPacoteviagemId);
    }

    /**
     * Test of getIdUtilizadorId method, of class Reserva.
     */
    @Test
    public void testGetIdUtilizadorId() {
        System.out.println("getIdUtilizadorId");
        Reserva instance = new Reserva();
        Utilizador expResult = null;
        Utilizador result = instance.getIdUtilizadorId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setIdUtilizadorId method, of class Reserva.
     */
    @Test
    public void testSetIdUtilizadorId() {
        System.out.println("setIdUtilizadorId");
        Utilizador idUtilizadorId = null;
        Reserva instance = new Reserva();
        instance.setIdUtilizadorId(idUtilizadorId);
    }

    /**
     * Test of hashCode method, of class Reserva.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Reserva instance = new Reserva();
        int expResult = 0;
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Reserva.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object object = null;
        Reserva instance = new Reserva();
        boolean expResult = false;
        boolean result = instance.equals(object);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Reserva.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Reserva instance = new Reserva();
        String expResult = "entities.Reserva[ id=" + instance.getId() + " ]";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
