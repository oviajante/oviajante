/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Collection;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rita
 */
public class UtilizadorTest {
    
    public UtilizadorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getId method, of class Utilizador.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");
        Utilizador instance = new Utilizador();
        Long expResult = null;
        Long result = instance.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setId method, of class Utilizador.
     */
    @Test
    public void testSetId() {
        System.out.println("setId");
        Long id = null;
        Utilizador instance = new Utilizador();
        instance.setId(id);
    }

    /**
     * Test of getData method, of class Utilizador.
     */
    @Test
    public void testGetData() {
        System.out.println("getData");
        Utilizador instance = new Utilizador();
        String expResult = null;
        String result = instance.getData();
        assertEquals(expResult, result);
    }

    /**
     * Test of setData method, of class Utilizador.
     */
    @Test
    public void testSetData() {
        System.out.println("setData");
        String data = "";
        Utilizador instance = new Utilizador();
        instance.setData(data);
    }

    /**
     * Test of getEmail method, of class Utilizador.
     */
    @Test
    public void testGetEmail() {
        System.out.println("getEmail");
        Utilizador instance = new Utilizador();
        String expResult = null;
        String result = instance.getEmail();
        assertEquals(expResult, result);
    }

    /**
     * Test of setEmail method, of class Utilizador.
     */
    @Test
    public void testSetEmail() {
        System.out.println("setEmail");
        String email = "";
        Utilizador instance = new Utilizador();
        instance.setEmail(email);
    }

    /**
     * Test of getNome method, of class Utilizador.
     */
    @Test
    public void testGetNome() {
        System.out.println("getNome");
        Utilizador instance = new Utilizador();
        String expResult = null;
        String result = instance.getNome();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNome method, of class Utilizador.
     */
    @Test
    public void testSetNome() {
        System.out.println("setNome");
        String nome = "";
        Utilizador instance = new Utilizador();
        instance.setNome(nome);
    }

    /**
     * Test of getPassword method, of class Utilizador.
     */
    @Test
    public void testGetPassword() {
        System.out.println("getPassword");
        Utilizador instance = new Utilizador();
        String expResult = null;
        String result = instance.getPassword();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPassword method, of class Utilizador.
     */
    @Test
    public void testSetPassword() {
        System.out.println("setPassword");
        String password = "";
        Utilizador instance = new Utilizador();
        instance.setPassword(password);
    }

    /**
     * Test of getTelemovel method, of class Utilizador.
     */
    @Test
    public void testGetTelemovel() {
        System.out.println("getTelemovel");
        Utilizador instance = new Utilizador();
        Integer expResult = null;
        Integer result = instance.getTelemovel();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTelemovel method, of class Utilizador.
     */
    @Test
    public void testSetTelemovel() {
        System.out.println("setTelemovel");
        Integer telemovel = null;
        Utilizador instance = new Utilizador();
        instance.setTelemovel(telemovel);
    }

    /**
     * Test of getUtilizador method, of class Utilizador.
     */
    @Test
    public void testGetUtilizador() {
        System.out.println("getUtilizador");
        Utilizador instance = new Utilizador();
        String expResult = null;
        String result = instance.getUtilizador();
        assertEquals(expResult, result);
    }

    /**
     * Test of setUtilizador method, of class Utilizador.
     */
    @Test
    public void testSetUtilizador() {
        System.out.println("setUtilizador");
        String utilizador = "";
        Utilizador instance = new Utilizador();
        instance.setUtilizador(utilizador);
    }

    /**
     * Test of getBlogentryCollection method, of class Utilizador.
     */
    @Test
    public void testGetBlogentryCollection() {
        System.out.println("getBlogentryCollection");
        Utilizador instance = new Utilizador();
        Collection<Blogentry> expResult = null;
        Collection<Blogentry> result = instance.getBlogentryCollection();
        assertEquals(expResult, result);
    }

    /**
     * Test of setBlogentryCollection method, of class Utilizador.
     */
    @Test
    public void testSetBlogentryCollection() {
        System.out.println("setBlogentryCollection");
        Collection<Blogentry> blogentryCollection = null;
        Utilizador instance = new Utilizador();
        instance.setBlogentryCollection(blogentryCollection);
    }

    /**
     * Test of getReservaCollection method, of class Utilizador.
     */
    @Test
    public void testGetReservaCollection() {
        System.out.println("getReservaCollection");
        Utilizador instance = new Utilizador();
        Collection<Reserva> expResult = null;
        Collection<Reserva> result = instance.getReservaCollection();
        assertEquals(expResult, result);
    }

    /**
     * Test of setReservaCollection method, of class Utilizador.
     */
    @Test
    public void testSetReservaCollection() {
        System.out.println("setReservaCollection");
        Collection<Reserva> reservaCollection = null;
        Utilizador instance = new Utilizador();
        instance.setReservaCollection(reservaCollection);
 
    }

    /**
     * Test of hashCode method, of class Utilizador.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Utilizador instance = new Utilizador();
        int expResult = 0;
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Utilizador.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object object = null;
        Utilizador instance = new Utilizador();
        boolean expResult = false;
        boolean result = instance.equals(object);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Utilizador.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Utilizador instance = new Utilizador();
        String expResult = "entities.Utilizador[ id=" + instance.getId() + " ]";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
