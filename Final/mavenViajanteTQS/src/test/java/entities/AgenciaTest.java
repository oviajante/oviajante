/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Collection;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rita
 */
public class AgenciaTest {
    
    public AgenciaTest() {
    }

    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getId method, of class Agencia.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");
        Agencia instance = new Agencia();
        Long expResult = null;
        Long result = instance.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setId method, of class Agencia.
     */
    @Test
    public void testSetId() {
        System.out.println("setId");
        Long id = null;
        Agencia instance = new Agencia();
        instance.setId(id);
    }

    /**
     * Test of getCompanhia method, of class Agencia.
     */
    @Test
    public void testGetCompanhia() {
        System.out.println("getCompanhia");
        Agencia instance = new Agencia();
        String expResult = null;
        String result = instance.getCompanhia();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCompanhia method, of class Agencia.
     */
    @Test
    public void testSetCompanhia() {
        System.out.println("setCompanhia");
        String companhia = "";
        Agencia instance = new Agencia();
        instance.setCompanhia(companhia);
    }

    /**
     * Test of getPassword method, of class Agencia.
     */
    @Test
    public void testGetPassword() {
        System.out.println("getPassword");
        Agencia instance = new Agencia();
        String expResult = null;
        String result = instance.getPassword();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPassword method, of class Agencia.
     */
    @Test
    public void testSetPassword() {
        System.out.println("setPassword");
        String password = "";
        Agencia instance = new Agencia();
        instance.setPassword(password);
    }

    /**
     * Test of getUtilizador method, of class Agencia.
     */
    @Test
    public void testGetUtilizador() {
        System.out.println("getUtilizador");
        Agencia instance = new Agencia();
        String expResult = null;
        String result = instance.getUtilizador();
        assertEquals(expResult, result);
    }

    /**
     * Test of setUtilizador method, of class Agencia.
     */
    @Test
    public void testSetUtilizador() {
        System.out.println("setUtilizador");
        String utilizador = "";
        Agencia instance = new Agencia();
        instance.setUtilizador(utilizador);
    }

    /**
     * Test of getPacoteviagemCollection method, of class Agencia.
     */
    @Test
    public void testGetPacoteviagemCollection() {
        System.out.println("getPacoteviagemCollection");
        Agencia instance = new Agencia();
        Collection<Pacoteviagem> expResult = null;
        Collection<Pacoteviagem> result = instance.getPacoteviagemCollection();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPacoteviagemCollection method, of class Agencia.
     */
    @Test
    public void testSetPacoteviagemCollection() {
        System.out.println("setPacoteviagemCollection");
        Collection<Pacoteviagem> pacoteviagemCollection = null;
        Agencia instance = new Agencia();
        instance.setPacoteviagemCollection(pacoteviagemCollection);
    }

    /**
     * Test of hashCode method, of class Agencia.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Agencia instance = new Agencia();
        int expResult = 0;
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Agencia.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object object = null;
        Agencia instance = new Agencia();
        boolean expResult = false;
        boolean result = instance.equals(object);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Agencia.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Agencia instance = new Agencia();
        String expResult = "entities.Agencia[ id=" + instance.getId() + " ]";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
