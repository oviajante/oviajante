/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rita
 */
public class BlogentryTest {
    
    public BlogentryTest() {
    }
    

    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getId method, of class Blogentry.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");
        Blogentry instance = new Blogentry();
        Long expResult = null;
        Long result = instance.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setId method, of class Blogentry.
     */
    @Test
    public void testSetId() {
        System.out.println("setId");
        Long id = null;
        Blogentry instance = new Blogentry();
        instance.setId(id);
    }

    /**
     * Test of getData method, of class Blogentry.
     */
    @Test
    public void testGetData() {
        System.out.println("getData");
        Blogentry instance = new Blogentry();
        String expResult = null;
        String result = instance.getData();
        assertEquals(expResult, result);
    }

    /**
     * Test of setData method, of class Blogentry.
     */
    @Test
    public void testSetData() {
        System.out.println("setData");
        String data = "";
        Blogentry instance = new Blogentry();
        instance.setData(data);
    }

    /**
     * Test of getImagemurl method, of class Blogentry.
     */
    @Test
    public void testGetImagemurl() {
        System.out.println("getImagemurl");
        Blogentry instance = new Blogentry();
        String expResult = null;
        String result = instance.getImagemurl();
        assertEquals(expResult, result);
    }

    /**
     * Test of setImagemurl method, of class Blogentry.
     */
    @Test
    public void testSetImagemurl() {
        System.out.println("setImagemurl");
        String imagemurl = "";
        Blogentry instance = new Blogentry();
        instance.setImagemurl(imagemurl);
    }

    /**
     * Test of getLocal method, of class Blogentry.
     */
    @Test
    public void testGetLocal() {
        System.out.println("getLocal");
        Blogentry instance = new Blogentry();
        String expResult = null;
        String result = instance.getLocal();
        assertEquals(expResult, result);
    }

    /**
     * Test of setLocal method, of class Blogentry.
     */
    @Test
    public void testSetLocal() {
        System.out.println("setLocal");
        String local = "";
        Blogentry instance = new Blogentry();
        instance.setLocal(local);
    }

    /**
     * Test of getTexto method, of class Blogentry.
     */
    @Test
    public void testGetTexto() {
        System.out.println("getTexto");
        Blogentry instance = new Blogentry();
        String expResult = null;
        String result = instance.getTexto();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTexto method, of class Blogentry.
     */
    @Test
    public void testSetTexto() {
        System.out.println("setTexto");
        String texto = "";
        Blogentry instance = new Blogentry();
        instance.setTexto(texto);
    }

    /**
     * Test of setTipoviagem method, of class Blogentry.
     */
    @Test
    public void testSetTipoviagem() {
        System.out.println("setTipoviagem");
        String tipoviagem = "";
        Blogentry instance = new Blogentry();
        instance.setTipoviagem(tipoviagem);
    }

    /**
     * Test of getTitulo method, of class Blogentry.
     */
    @Test
    public void testGetTitulo() {
        System.out.println("getTitulo");
        Blogentry instance = new Blogentry();
        String expResult = null;
        String result = instance.getTitulo();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTitulo method, of class Blogentry.
     */
    @Test
    public void testSetTitulo() {
        System.out.println("setTitulo");
        String titulo = "";
        Blogentry instance = new Blogentry();
        instance.setTitulo(titulo);
    }

    /**
     * Test of getIdUtilizadorId method, of class Blogentry.
     */
    @Test
    public void testGetIdUtilizadorId() {
        System.out.println("getIdUtilizadorId");
        Blogentry instance = new Blogentry();
        Utilizador expResult = null;
        Utilizador result = instance.getIdUtilizadorId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setIdUtilizadorId method, of class Blogentry.
     */
    @Test
    public void testSetIdUtilizadorId() {
        System.out.println("setIdUtilizadorId");
        Utilizador idUtilizadorId = null;
        Blogentry instance = new Blogentry();
        instance.setIdUtilizadorId(idUtilizadorId);
    }

    /**
     * Test of hashCode method, of class Blogentry.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Blogentry instance = new Blogentry();
        int expResult = 0;
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Blogentry.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object object = null;
        Blogentry instance = new Blogentry();
        boolean expResult = false;
        boolean result = instance.equals(object);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Blogentry.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Blogentry instance = new Blogentry();
        String expResult = "entities.Blogentry[ id=" + instance.getId() + " ]";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
