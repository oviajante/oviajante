//package selenium;
//
//import java.util.regex.Pattern;
//import java.util.concurrent.TimeUnit;
//import org.junit.*;
//import static org.junit.Assert.*;
//import static org.hamcrest.CoreMatchers.*;
//import org.openqa.selenium.*;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.support.ui.Select;
//
//public class AddPacoteAgenciaSuccessTest {
//
//    private WebDriver driver;
//    private String baseUrl;
//    private boolean acceptNextAlert = true;
//    private StringBuffer verificationErrors = new StringBuffer();
//
//    @Before
//    public void setUp() throws Exception {
//        System.setProperty("webdriver.chrome.driver", "chromedriver");
//        driver = new ChromeDriver();
//        baseUrl = "localhost:8080";
//        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//    }
//
//    @Test
//    public void testAddPacoteAgenciaSuccess() throws Exception {
//        driver.get(baseUrl + "/mavenViajanteTQS/faces/login.xhtml");
//        driver.findElement(By.linkText("Adicionar Pacote")).click();
//        driver.findElement(By.id("j_idt15:titulo")).clear();
//        driver.findElement(By.id("j_idt15:titulo")).sendKeys("Paris e Londres");
//        driver.findElement(By.id("j_idt15:preco")).clear();
//        driver.findElement(By.id("j_idt15:preco")).sendKeys("1000£");
//        driver.findElement(By.id("j_idt15:tipo")).clear();
//        driver.findElement(By.id("j_idt15:tipo")).sendKeys("cruzeiros");
//        driver.findElement(By.id("pacotes")).click();
//        driver.findElement(By.cssSelector("#maincontainer > h1")).click();
//        driver.findElement(By.id("j_idt15:alojamento")).clear();
//        driver.findElement(By.id("j_idt15:alojamento")).sendKeys("3 dias e 2 noites");
//        driver.findElement(By.id("j_idt15:itenerario")).clear();
//        driver.findElement(By.id("j_idt15:itenerario")).sendKeys("Dia 1: dia livre, Dia 2: arco do triunfo, Dia3: dia livre");
//        driver.findElement(By.id("j_idt15:condi")).clear();
//        driver.findElement(By.id("j_idt15:condi")).sendKeys("Hotel NH 4*");
//        driver.findElement(By.id("j_idt15:detalhe")).clear();
//        driver.findElement(By.id("j_idt15:detalhe")).sendKeys("Europa aos melhores preços");
//        driver.findElement(By.id("j_idt15:j_idt24")).click();
//        driver.findElement(By.id("pacotes")).click();
//        driver.findElement(By.linkText("Cruzeiros")).click();
//        assertEquals("Paris e Londres", driver.findElement(By.xpath("//div[@id='form:pacote_content']/div[2]/div[3]/div/p")).getText());
//        assertEquals("CEDIDO POR:", driver.findElement(By.xpath("//div[@id='form:pacote_content']/div[2]/div[3]/div/div/p")).getText());
//        assertEquals("1000£", driver.findElement(By.xpath("//div[@id='form:pacote_content']/div[2]/div[3]/div/div[2]/h3")).getText());
//    }
//
//    @After
//    public void tearDown() throws Exception {
//        driver.quit();
//        String verificationErrorString = verificationErrors.toString();
//        if (!"".equals(verificationErrorString)) {
//            fail(verificationErrorString);
//        }
//    }
//
//    private boolean isElementPresent(By by) {
//        try {
//            driver.findElement(by);
//            return true;
//        } catch (NoSuchElementException e) {
//            return false;
//        }
//    }
//
//    private boolean isAlertPresent() {
//        try {
//            driver.switchTo().alert();
//            return true;
//        } catch (NoAlertPresentException e) {
//            return false;
//        }
//    }
//
//    private String closeAlertAndGetItsText() {
//        try {
//            Alert alert = driver.switchTo().alert();
//            String alertText = alert.getText();
//            if (acceptNextAlert) {
//                alert.accept();
//            } else {
//                alert.dismiss();
//            }
//            return alertText;
//        } finally {
//            acceptNextAlert = true;
//        }
//    }
//}
