//package selenium;
//
//import java.util.regex.Pattern;
//import java.util.concurrent.TimeUnit;
//import org.junit.*;
//import static org.junit.Assert.*;
//import static org.hamcrest.CoreMatchers.*;
//import org.openqa.selenium.*;
//import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.support.ui.Select;
//
//public class ShareBlogPostSuccessTest {
//  private WebDriver driver;
//  private String baseUrl;
//  private boolean acceptNextAlert = true;
//  private StringBuffer verificationErrors = new StringBuffer();
//
//  @Before
//  public void setUp() throws Exception {
//    driver = new FirefoxDriver();
//    baseUrl = "localhost:8080";
//    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//  }
//
//  @Test
//  public void testShareBlogPostSuccess() throws Exception {
//    driver.get(baseUrl + "/mavenViajanteTQS/faces/login.xhtml");
//    driver.findElement(By.linkText("Partilhar Blog")).click();
//    driver.findElement(By.id("j_idt15:titulo")).clear();
//    driver.findElement(By.id("j_idt15:titulo")).sendKeys("Amsterdam");
//    driver.findElement(By.id("j_idt15:data")).clear();
//    driver.findElement(By.id("j_idt15:data")).sendKeys("12/12/12");
//    driver.findElement(By.id("j_idt15:tipo")).clear();
//    driver.findElement(By.id("j_idt15:tipo")).sendKeys("Sozinho");
//    driver.findElement(By.id("j_idt15:local")).clear();
//    driver.findElement(By.id("j_idt15:local")).sendKeys("Europa");
//    driver.findElement(By.id("j_idt15:texto")).clear();
//    driver.findElement(By.id("j_idt15:texto")).sendKeys("Cidade Maravilhosa, as pessoas locais sao muito simpáticas. 3 dias maravilhosos. Recomendo.");
//    driver.findElement(By.id("j_idt15:j_idt22")).click();
//    driver.findElement(By.id("experiencias")).click();
//    assertEquals("Amsterdam", driver.findElement(By.linkText("Amsterdam")).getText());
//    assertEquals("Amsterdam by: Tiago Teixeira - Viagem em Grupo \n 12/12/12\n \n \n Cidade Maravilhosa, as pessoas locais sao muito simpáticas. 3 dias maravilhosos. Recomendo.", driver.findElement(By.xpath("//div[@id='j_idt33:j_idt35']/div/ul/li[28]/div")).getText());
//  }
//
//  @After
//  public void tearDown() throws Exception {
//    driver.quit();
//    String verificationErrorString = verificationErrors.toString();
//    if (!"".equals(verificationErrorString)) {
//      fail(verificationErrorString);
//    }
//  }
//
//  private boolean isElementPresent(By by) {
//    try {
//      driver.findElement(by);
//      return true;
//    } catch (NoSuchElementException e) {
//      return false;
//    }
//  }
//
//  private boolean isAlertPresent() {
//    try {
//      driver.switchTo().alert();
//      return true;
//    } catch (NoAlertPresentException e) {
//      return false;
//    }
//  }
//
//  private String closeAlertAndGetItsText() {
//    try {
//      Alert alert = driver.switchTo().alert();
//      String alertText = alert.getText();
//      if (acceptNextAlert) {
//        alert.accept();
//      } else {
//        alert.dismiss();
//      }
//      return alertText;
//    } finally {
//      acceptNextAlert = true;
//    }
//  }
//}
