/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.presentation;

import boundary.AgenciaFacade;
import boundary.UtilizadorFacade;
import entities.Agencia;
import entities.Utilizador;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author rui
 */
@Named(value = "registarView")
@Dependent
public class registarView {

    @EJB
    private UtilizadorFacade utilizadorFacade;
    @EJB
    private AgenciaFacade agenciaFacade;
    public static String username = "";
    private static String warning_login = "";
    private static String warning_signup = "";
    static String tipoRegisto = new String("user");

    /**
     * Creates a new instance of SignupView
     */
    public registarView() {
        utilizadorFacade = new UtilizadorFacade();
    }

    public String getTipoRegisto() {
        FacesContext fc = FacesContext.getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        tipoRegisto = params.get("tipoRegisto");
        return tipoRegisto;
    }

    public boolean isUtilizador() {
        FacesContext fc = FacesContext.getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        tipoRegisto = params.get("tipoRegisto");
        return (tipoRegisto.equals("user"));
    }

    public boolean isAgencia() {
        return (tipoRegisto.equals("agencia"));
    }

    public String getUsername() {
        return username;
    }

    public String getWarning_login() {
        return warning_login;
    }

    public String getWarning_signup() {
        return warning_signup;
    }

    public void signupUser(String nomeCompleto, String telemovel, String email, String password, String username) throws IOException {

        boolean valido = true;

        if (!username.equals("") && !password.equals("") && !email.equals("")) {
            List<Utilizador> players = utilizadorFacade.findAll();

            for (Utilizador player : players) {
                if (player.getEmail().equals(email)) {
                    valido = false;
                }
                warning_signup = "EMUSO";
            }

        } else {
            valido = false;

        }

        if (valido) {
            Utilizador ui = new Utilizador();

            //ui.setId(new Long(200));
            ui.setNome(nomeCompleto);
            ui.setData("06-05-1994");
            ui.setTelemovel(Integer.parseInt(telemovel));
            ui.setEmail(email);
            ui.setUtilizador(username);
            ui.setPassword(password);
            System.out.print(ui.toString());

            utilizadorFacade.create(ui);
            this.username = username;

            String uri = "home.xhtml";
            FacesContext.getCurrentInstance().getExternalContext().dispatch(uri);

        } else if (warning_signup.equals("EMUSO")) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Email ja se encontra em uso!"));
        } else {

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Insira as credenciais correctamente!"));

        }

    }

    public void signupAgencia(String companhia, String username, String password) throws IOException {

        boolean valido = true;

        if (!companhia.equals("") && !password.equals("") && !username.equals("")) {
            List<Agencia> players = agenciaFacade.findAll();

            for (Agencia player : players) {
                if (player.getUtilizador().equals(username)) {
                    valido = false;
                }
                warning_signup = "EMUSO";
            }

        } else {
            valido = false;

        }

        if (valido) {
            Agencia ui = new Agencia();

            //ui.setId(new Long(200));
            ui.setCompanhia(companhia);
            ui.setUtilizador(username);
            ui.setPassword(password);
            System.out.print(ui.toString());

            agenciaFacade.create(ui);
            this.username = username;

            String uri = "home.xhtml";
            FacesContext.getCurrentInstance().getExternalContext().dispatch(uri);

        } else if (warning_signup.equals("EMUSO")) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Username ja se encontra em uso!"));
        } else {

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Insira as credenciais correctamente!"));

        }

    }

    public void logout() throws IOException {
        // this.username = "";
        // TeamView.admin = "";
        // ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        // ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
    }

    public void login(String username, String password) throws IOException {
        boolean valido = false;

        for (Utilizador p : utilizadorFacade.findAll()) {
            if (p.getEmail().equals(username) && p.getPassword().equals(password)) {
                valido = true;
                this.username = username;
            }
        }

        if (valido) {
            warning_login = "";

            String uri = "home.xhtml";
            FacesContext.getCurrentInstance().getExternalContext().dispatch(uri);

        } else {
            warning_login = "Your data are wrong!";
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Credenciais Invalidas!"));
        }
    }
}
