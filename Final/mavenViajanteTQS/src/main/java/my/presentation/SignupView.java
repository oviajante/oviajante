/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.presentation;

import boundary.AgenciaFacade;
import boundary.UtilizadorFacade;
import entities.Agencia;
import entities.Utilizador;
import java.io.IOException;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationHandler;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author rui
 */
@Named(value = "signupView")
@Dependent
public class SignupView {

    @EJB
    private UtilizadorFacade utilizadorFacade;
    @EJB
    private AgenciaFacade agenciaFacade;

    public static String username = "teste";
    public static Long userlongID;
    private static String warning_login = "teste";
    private static String warning_signup = "teste";
    public static boolean login = false;

    public static boolean loginisClient = false;
    public static boolean loginisAgencia = false;

    public boolean getLoginagencia() {
        return loginisAgencia;
    }

    public boolean getLoginclient() {
        return loginisClient;
    }

    /**
     * Creates a new instance of SignupView
     */
    public SignupView() {
        utilizadorFacade = new UtilizadorFacade();

        agenciaFacade = new AgenciaFacade();
    }

    public String getUsername() {
        return username;
    }

    public String getWarning_login() {
        return warning_login;
    }

    public String getWarning_signup() {
        return warning_signup;
    }

    public boolean getLogin() {
        return login;
    }

    public void loginasd(String username, String password) throws IOException {
        boolean valido = false;

        for (Utilizador p : utilizadorFacade.findAll()) {
            if (p.getEmail().equals(username) && p.getPassword().equals(password)) {
                valido = true;
                this.username = p.getNome();
                this.userlongID = p.getId();
                this.loginisClient = true;
                this.loginisAgencia = false;
            }
        }

        if (!loginisClient) {

            for (Agencia a : agenciaFacade.findAll()) {
                if (a.getUtilizador().equals(username) && a.getPassword().equals(password)) {
                    valido = true;
                    this.username = a.getCompanhia();
                    this.userlongID = a.getId();
                    this.loginisClient = false;
                    this.loginisAgencia = true;
                }
            }

        }

        if (valido) {
            warning_login = "";
            login = true;
            String uri = "home.xhtml";
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", username);
            //<h:outputText value="#{sessionScope.user}" /> para ir buscar
            FacesContext.getCurrentInstance().getExternalContext().dispatch(uri);

        } else {
            warning_login = "Your data are wrong!";
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Credenciais Invalidas!"));
        }
    }

    public static Long getUserlongID() {
        return userlongID;
    }

    public static void setUserlongID(Long userlongID) {
        SignupView.userlongID = userlongID;
    }

    public void logout() throws IOException {
        username = "";
        login = false;

        loginisClient = false;
        loginisAgencia = false;
        String uri = "home.xhtml";
        FacesContext.getCurrentInstance().getExternalContext().dispatch(uri);
    }

}
