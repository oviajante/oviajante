/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.presentation;

import boundary.BlogentryFacade;
import boundary.UtilizadorFacade;
import entities.Blogentry;
import entities.Utilizador;
import java.io.IOException;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author rui
 */
@Named(value = "partilharblogView")
@Dependent
public class PartilharBlogView {

    @EJB
    private BlogentryFacade blogentryFacade;

    @EJB
    private UtilizadorFacade utilizadorFacade;

    private String optionlocal;
    private String optiontipo;

    /**
     * Creates a new instance of SignupView
     */
    public PartilharBlogView() {
        blogentryFacade = new BlogentryFacade();
        utilizadorFacade = new UtilizadorFacade();
    }

    public String getOptionlocal() {
        return optionlocal;
    }

    public void setOptionlocal(String optionlocal) {
        this.optionlocal = optionlocal;
    }

    public String getOptiontipo() {
        return optiontipo;
    }

    public void setOptiontipo(String optiontipo) {
        this.optiontipo = optiontipo;
    }

    public void newEntry(String titulo, String data, String texto, String local, String tipo, String imagem) throws IOException {

        boolean valido = true;

        if (valido) {
            Blogentry ui = new Blogentry();

            //ui.setId(new Long(200));
            Long as = SignupView.getUserlongID();
            
            Utilizador uti = utilizadorFacade.find(as);
            
            
            ui.setTitulo(titulo);
            ui.setIdUtilizadorId(uti);
            ui.setData(data);
            ui.setLocal(local);
            ui.setImagemurl(imagem);
            ui.setTexto(texto);
            ui.setTipoviagem(tipo);

            System.out.print(ui.toString());

            blogentryFacade.create(ui);

            String uri = "home.xhtml";
            FacesContext.getCurrentInstance().getExternalContext().dispatch(uri);

        } else {

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Insira as informacoes correctamente!"));

        }

    }
}
