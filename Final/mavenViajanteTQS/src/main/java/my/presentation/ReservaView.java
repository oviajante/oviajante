/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.presentation;

import boundary.AgenciaFacade;
import boundary.PacoteviagemFacade;
import boundary.ReservaFacade;
import boundary.UtilizadorFacade;
import entities.Pacoteviagem;
import entities.Reserva;
import entities.Utilizador;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author sarafurao
 */
@Named(value = "ReservaView")
@RequestScoped
public class ReservaView {

    @EJB
    private ReservaFacade reservaFacade;

    @EJB
    private UtilizadorFacade utilizadorFacade;

    @EJB
    private PacoteviagemFacade pacviagemFacade;

    
    @EJB
    private AgenciaFacade agenciaFacade;

    
    Reserva reserva;

    public ReservaView() {

    }

    public ReservaView(Reserva reserva) {
        this.reserva = reserva;
    }

    public String getIdPacote() {
        FacesContext fc = FacesContext.getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        String id = params.get("pacoteid");
        return id;
    }

    public void efectuarReserva() throws IOException {

        if (SignupView.loginisClient) {

            Reserva reserva = new Reserva();

            reserva.setDatapartida("29-03-2016");

            Pacoteviagem pacv = pacviagemFacade.find(new Long(Integer.parseInt(getIdPacote())));
            reserva.setIdPacoteviagemId(pacv);
            
            Utilizador uti = utilizadorFacade.find(SignupView.userlongID);
            reserva.setIdUtilizadorId(uti);
            
            
            reservaFacade.create(reserva);

            String uri = "home.xhtml";
            FacesContext.getCurrentInstance().getExternalContext().dispatch(uri);
        } else {
            String uri = "login.xhtml";
            FacesContext.getCurrentInstance().getExternalContext().dispatch(uri);
        }
    }
    
       public List<Reserva> getReservasAgencia() {
        return reservaFacade.getReservasOf(agenciaFacade.find(SignupView.userlongID));
    }
       

}
