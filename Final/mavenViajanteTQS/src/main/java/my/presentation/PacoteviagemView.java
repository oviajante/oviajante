/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.presentation;

import boundary.AgenciaFacade;
import boundary.PacoteviagemFacade;
import entities.Agencia;
import entities.Blogentry;
import entities.Pacoteviagem;
import entities.Utilizador;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author sarafurao
 */
@Named(value = "PacoteviagemView")
@RequestScoped
public class PacoteviagemView {

    @EJB
    private PacoteviagemFacade pacoteviagemFacade;

    @EJB
    private AgenciaFacade agenciaFacade;

    String tipoPacote = new String();
    String selectedPacote = new String();

    Pacoteviagem pacoteviagem;

    public void newEntry(String titulo, String preco, String tipo, String alojamento, String itenetario, String condicoes, String detalhe) throws IOException {

        boolean valido = true;

        if (valido) {

            Pacoteviagem ui = new Pacoteviagem();

            //ui.setId(new Long(200));
            Long agencia = SignupView.getUserlongID();

            Agencia uti = agenciaFacade.find(agencia);

            
            ui.setTitulo(titulo);
            ui.setAlojamento(alojamento);
            ui.setCondicoes(condicoes);
            ui.setDetalhe(detalhe);
            ui.setImagemurl("https://www.dropbox.com/s/vhmozq3io8lof9i/mauricias.jpg?raw=1");
            ui.setItenerario(itenetario);
            ui.setPreco(preco);
            ui.setTipopacote(tipo);
            
            ui.setIdAgenciaId(uti);
            
            System.out.print(ui.toString());

            pacoteviagemFacade.create(ui);

            String uri = "home.xhtml";
            FacesContext.getCurrentInstance().getExternalContext().dispatch(uri);

        } else {

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Insira as informacoes correctamente!"));

        }

    }

    public PacoteviagemView() {

        pacoteviagemFacade = new PacoteviagemFacade();
        agenciaFacade = new AgenciaFacade();
    }

    public PacoteviagemView(Pacoteviagem pacoteviagem) {
        this.pacoteviagem = pacoteviagem;
    }

    public String getTipoPacote() {
        FacesContext fc = FacesContext.getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        tipoPacote = params.get("tipopacote");
        return tipoPacote;
    }

    public List<Pacoteviagem> getPacotes(String tipoPacote) {
        if (tipoPacote.equals("Cruzeiros")) {
            tipoPacote = "cruzeiros";
        } else if (tipoPacote.equals("Disneyland")) {
            tipoPacote = "disney";
        } else if (tipoPacote.equals("Praias e Ilhas")) {
            tipoPacote = "praias";
        } else if (tipoPacote.equals("Escapadinhas")) {
            tipoPacote = "escapadinhas";
        } else if (tipoPacote.equals("Grandes Viagens")) {
            tipoPacote = "grandesViagens";
        } else {
            tipoPacote = "promocoes";
        }
        return pacoteviagemFacade.getPacotesTipo(tipoPacote);
    }

    public String getSelectedPacote() {
        FacesContext fc = FacesContext.getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        selectedPacote = params.get("selectedPacote");
        return selectedPacote;
    }

    public Pacoteviagem getDetailPacote(String id) {
        return pacoteviagemFacade.find(new Long(Integer.parseInt(id)));
    }

}
