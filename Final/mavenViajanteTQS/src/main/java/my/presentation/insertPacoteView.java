/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.presentation;

import boundary.AgenciaFacade;
import boundary.BlogentryFacade;
import boundary.PacoteviagemFacade;
import boundary.UtilizadorFacade;
import entities.Agencia;
import entities.Blogentry;
import entities.Pacoteviagem;
import entities.Utilizador;
import java.io.IOException;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author rui
 */
@Named(value = "insertPacoteView")
@Dependent
public class insertPacoteView {

    @EJB
    private PacoteviagemFacade pacoteviagemFacade;

    @EJB
    private AgenciaFacade agenciaFacade;

    /**
     * Creates a new instance of SignupView
     */
    public insertPacoteView() {
        pacoteviagemFacade = new PacoteviagemFacade();
        agenciaFacade = new AgenciaFacade();
    }

    public void newEntry(String titulo, String preco, String tipo, String alojamento, String itenetario, String condicoes, String detalhe, String imagem) throws IOException {

        boolean valido = true;

        if (valido) {

            Pacoteviagem pac = new Pacoteviagem();
            //pac.setId(new Long(253));
            pac.setAlojamento(alojamento);
            pac.setCondicoes(condicoes);
            pac.setDetalhe(detalhe);
            pac.setImagemurl(imagem);
            pac.setItenerario(itenetario);

            pac.setPreco(preco);
            pac.setTipopacote(tipo);
            pac.setTitulo(titulo);

            Long agencia = SignupView.getUserlongID();

            Agencia a = agenciaFacade.find(agencia);
            pac.setIdAgenciaId(a);

            
            
            pacoteviagemFacade.create(pac);

            String uri = "home.xhtml";
            FacesContext.getCurrentInstance().getExternalContext().dispatch(uri);

        } else {

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Insira as informacoes correctamente!"));

        }

    }
}
