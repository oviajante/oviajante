/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary;

import entities.Pacoteviagem;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author sarafurao
 */
@Stateless
public class PacoteviagemFacade extends AbstractFacade<Pacoteviagem> {

    @PersistenceContext(unitName = "com.mycompany_mavenViajanteTQS_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PacoteviagemFacade() {
        super(Pacoteviagem.class);
    }

    public List<Pacoteviagem> getPacotesTipo(String tipo) {
        TypedQuery<Pacoteviagem> query = em.createQuery("SELECT g FROM Pacoteviagem g WHERE g.tipopacote=:tipo", Pacoteviagem.class);
        query.setParameter("tipo", tipo);
        return query.getResultList();
    }
}
