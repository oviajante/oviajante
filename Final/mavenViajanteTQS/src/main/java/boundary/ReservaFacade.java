/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary;

import entities.Agencia;
import entities.Reserva;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author sarafurao
 */
@Stateless
public class ReservaFacade extends AbstractFacade<Reserva> {

    @PersistenceContext(unitName = "com.mycompany_mavenViajanteTQS_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ReservaFacade() {
        super(Reserva.class);
    }

    public List<Reserva> getReservasOf(Agencia agencia) {
        TypedQuery<Reserva> query = em.createQuery("SELECT g FROM Reserva g JOIN g.idPacoteviagemId e WHERE e.idAgenciaId=:agencia", Reserva.class);
        query.setParameter("agencia", agencia);
        return query.getResultList();
    }

}
