/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary;

import entities.Blogentry;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author sarafurao
 */
@Stateless
public class BlogentryFacade extends AbstractFacade<Blogentry> {

    @PersistenceContext(unitName = "com.mycompany_mavenViajanteTQS_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BlogentryFacade() {
        super(Blogentry.class);
    }

}
