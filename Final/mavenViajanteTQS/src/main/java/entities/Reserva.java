/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author tiagoteixeira
 */
@Entity
@Table(name = "RESERVA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reserva.findAll", query = "SELECT r FROM Reserva r"),
    @NamedQuery(name = "Reserva.findById", query = "SELECT r FROM Reserva r WHERE r.id = :id"),
    @NamedQuery(name = "Reserva.findByDatapartida", query = "SELECT r FROM Reserva r WHERE r.datapartida = :datapartida")})
public class Reserva implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Size(max = 255)
    @Column(name = "DATAPARTIDA")
    private String datapartida;
    @JoinColumn(name = "ID_PACOTEVIAGEM_ID", referencedColumnName = "ID")
    @ManyToOne
    private Pacoteviagem idPacoteviagemId;
    @JoinColumn(name = "ID_UTILIZADOR_ID", referencedColumnName = "ID")
    @ManyToOne
    private Utilizador idUtilizadorId;

    public Reserva() {
    }

    public Reserva(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDatapartida() {
        return datapartida;
    }

    public void setDatapartida(String datapartida) {
        this.datapartida = datapartida;
    }

    public Pacoteviagem getIdPacoteviagemId() {
        return idPacoteviagemId;
    }

    public void setIdPacoteviagemId(Pacoteviagem idPacoteviagemId) {
        this.idPacoteviagemId = idPacoteviagemId;
    }

    public Utilizador getIdUtilizadorId() {
        return idUtilizadorId;
    }

    public void setIdUtilizadorId(Utilizador idUtilizadorId) {
        this.idUtilizadorId = idUtilizadorId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reserva)) {
            return false;
        }
        Reserva other = (Reserva) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Reserva[ id=" + id + " ]";
    }

}
