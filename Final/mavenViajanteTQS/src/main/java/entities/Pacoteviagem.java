/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author tiagoteixeira
 */
@Entity
@Table(name = "PACOTEVIAGEM")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pacoteviagem.findAll", query = "SELECT p FROM Pacoteviagem p"),
    @NamedQuery(name = "Pacoteviagem.findById", query = "SELECT p FROM Pacoteviagem p WHERE p.id = :id"),
    @NamedQuery(name = "Pacoteviagem.findByAlojamento", query = "SELECT p FROM Pacoteviagem p WHERE p.alojamento = :alojamento"),
    @NamedQuery(name = "Pacoteviagem.findByCondicoes", query = "SELECT p FROM Pacoteviagem p WHERE p.condicoes = :condicoes"),
    @NamedQuery(name = "Pacoteviagem.findByDetalhe", query = "SELECT p FROM Pacoteviagem p WHERE p.detalhe = :detalhe"),
    @NamedQuery(name = "Pacoteviagem.findByImagemurl", query = "SELECT p FROM Pacoteviagem p WHERE p.imagemurl = :imagemurl"),
    @NamedQuery(name = "Pacoteviagem.findByItenerario", query = "SELECT p FROM Pacoteviagem p WHERE p.itenerario = :itenerario"),
    @NamedQuery(name = "Pacoteviagem.findByPreco", query = "SELECT p FROM Pacoteviagem p WHERE p.preco = :preco"),
    @NamedQuery(name = "Pacoteviagem.findByTipopacote", query = "SELECT p FROM Pacoteviagem p WHERE p.tipopacote = :tipopacote"),
    @NamedQuery(name = "Pacoteviagem.findByTitulo", query = "SELECT p FROM Pacoteviagem p WHERE p.titulo = :titulo")})
public class Pacoteviagem implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Size(max = 255)
    @Column(name = "ALOJAMENTO")
    private String alojamento;
    @Size(max = 255)
    @Column(name = "CONDICOES")
    private String condicoes;
    @Size(max = 255)
    @Column(name = "DETALHE")
    private String detalhe;
    @Size(max = 255)
    @Column(name = "IMAGEMURL")
    private String imagemurl;
    @Size(max = 255)
    @Column(name = "ITENERARIO")
    private String itenerario;
    @Size(max = 255)
    @Column(name = "PRECO")
    private String preco;
    @Size(max = 255)
    @Column(name = "TIPOPACOTE")
    private String tipopacote;
    @Size(max = 255)
    @Column(name = "TITULO")
    private String titulo;
    @OneToMany(mappedBy = "idPacoteviagemId")
    private Collection<Reserva> reservaCollection;
    @JoinColumn(name = "ID_AGENCIA_ID", referencedColumnName = "ID")
    @ManyToOne
    private Agencia idAgenciaId;

    public Pacoteviagem() {
    }

    public Pacoteviagem(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAlojamento() {
        return alojamento;
    }

    public void setAlojamento(String alojamento) {
        this.alojamento = alojamento;
    }

    public String getCondicoes() {
        return condicoes;
    }

    public void setCondicoes(String condicoes) {
        this.condicoes = condicoes;
    }

    public String getDetalhe() {
        return detalhe;
    }

    public void setDetalhe(String detalhe) {
        this.detalhe = detalhe;
    }

    public String getImagemurl() {
        return imagemurl;
    }

    public void setImagemurl(String imagemurl) {
        this.imagemurl = imagemurl;
    }

    public String getItenerario() {
        return itenerario;
    }

    public void setItenerario(String itenerario) {
        this.itenerario = itenerario;
    }

    public String getPreco() {
        return preco;
    }

    public void setPreco(String preco) {
        this.preco = preco;
    }

    public String getTipopacote() {
        return tipopacote;
    }

    public void setTipopacote(String tipopacote) {
        this.tipopacote = tipopacote;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    @XmlTransient
    public Collection<Reserva> getReservaCollection() {
        return reservaCollection;
    }

    public void setReservaCollection(Collection<Reserva> reservaCollection) {
        this.reservaCollection = reservaCollection;
    }

    public Agencia getIdAgenciaId() {
        return idAgenciaId;
    }

    public void setIdAgenciaId(Agencia idAgenciaId) {
        this.idAgenciaId = idAgenciaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pacoteviagem)) {
            return false;
        }
        Pacoteviagem other = (Pacoteviagem) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Pacoteviagem[ id=" + id + " ]";
    }

}
