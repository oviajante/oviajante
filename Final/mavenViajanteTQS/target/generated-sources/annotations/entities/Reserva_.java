package entities;

import entities.Pacoteviagem;
import entities.Utilizador;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-06-13T00:24:51")
@StaticMetamodel(Reserva.class)
public class Reserva_ { 

    public static volatile SingularAttribute<Reserva, String> datapartida;
    public static volatile SingularAttribute<Reserva, Long> id;
    public static volatile SingularAttribute<Reserva, Pacoteviagem> idPacoteviagemId;
    public static volatile SingularAttribute<Reserva, Utilizador> idUtilizadorId;

}