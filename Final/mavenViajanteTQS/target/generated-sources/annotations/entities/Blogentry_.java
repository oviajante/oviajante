package entities;

import entities.Utilizador;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-06-13T00:24:51")
@StaticMetamodel(Blogentry.class)
public class Blogentry_ { 

    public static volatile SingularAttribute<Blogentry, String> texto;
    public static volatile SingularAttribute<Blogentry, String> data;
    public static volatile SingularAttribute<Blogentry, String> tipoviagem;
    public static volatile SingularAttribute<Blogentry, String> titulo;
    public static volatile SingularAttribute<Blogentry, Long> id;
    public static volatile SingularAttribute<Blogentry, String> imagemurl;
    public static volatile SingularAttribute<Blogentry, String> local;
    public static volatile SingularAttribute<Blogentry, Utilizador> idUtilizadorId;

}