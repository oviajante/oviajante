package entities;

import entities.Pacoteviagem;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-06-13T00:24:51")
@StaticMetamodel(Agencia.class)
public class Agencia_ { 

    public static volatile SingularAttribute<Agencia, String> password;
    public static volatile SingularAttribute<Agencia, String> utilizador;
    public static volatile CollectionAttribute<Agencia, Pacoteviagem> pacoteviagemCollection;
    public static volatile SingularAttribute<Agencia, Long> id;
    public static volatile SingularAttribute<Agencia, String> companhia;

}