/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oviajante;

/**
 *
 * @author Rui
 */
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import javax.xml.parsers.*;
import org.w3c.dom.*;

public class ParseXMLDOM {

    public static String companhia = "";
    public static String id = "";
    public static String utilizador = "";
    public static String password = "";

    public static boolean parseAgenciaLogin(String username, String password) {
        String url = "http://deti-tqs-vm6.ua.pt:8080/mavenViajanteTQS-1.0-SNAPSHOT/webresources/entities.agencia";
        try {
            DocumentBuilderFactory f
                    = DocumentBuilderFactory.newInstance();
            DocumentBuilder b = f.newDocumentBuilder();
            Document doc = b.parse(url);

            doc.getDocumentElement().normalize();
            System.out.println("Root element: "
                    + doc.getDocumentElement().getNodeName());

            // loop through each item
            NodeList items = doc.getElementsByTagName("agencia");
            for (int i = 0; i < items.getLength(); i++) {
                Node n = items.item(i);
                if (n.getNodeType() != Node.ELEMENT_NODE) {
                    continue;
                }
                Element e = (Element) n;

                // get the "title elem" in this item (only one)
                NodeList titleList
                        = e.getElementsByTagName("utilizador");
                Element titleElem = (Element) titleList.item(0);

                // get the "text node" in the title (only one)
                Node titleNode = titleElem.getChildNodes().item(0);
                utilizador = titleNode.getNodeValue();

                NodeList titleListpass
                        = e.getElementsByTagName("password");
                Element titleElempass = (Element) titleListpass.item(0);

                // get the "text node" in the title (only one)
                Node titleNodepass = titleElempass.getChildNodes().item(0);
                password = titleNodepass.getNodeValue();

                NodeList titleListcompanhia
                        = e.getElementsByTagName("companhia");
                Element titleElemcompanhia = (Element) titleListcompanhia.item(0);

                // get the "text node" in the title (only one)
                Node titleNodecompanhia = titleElemcompanhia.getChildNodes().item(0);
                companhia = titleNodecompanhia.getNodeValue();

                NodeList titleListid
                        = e.getElementsByTagName("id");
                Element titleElemid = (Element) titleListid.item(0);

                // get the "text node" in the title (only one)
                Node titleNodeid = titleElemid.getChildNodes().item(0);
                id = titleNodeid.getNodeValue();

                if (titleNode.getNodeValue().equalsIgnoreCase(username) && titleNodepass.getNodeValue().equalsIgnoreCase(password)) {
                    return true;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static String checkemptyID() throws ProtocolException, MalformedURLException, IOException {

        String url = "http://deti-tqs-vm6.ua.pt:8080/mavenViajanteTQS-1.0-SNAPSHOT/webresources/entities.pacoteviagem/";

        boolean urlvalido = false;
        int i = 255;
        do {
            i++;
            String tmp = url + Integer.toString(i);
            HttpURLConnection connection = (HttpURLConnection) new URL(tmp).openConnection();
            connection.setRequestMethod("HEAD");
            int responseCode = connection.getResponseCode();
            if (responseCode != 200) {
                System.out.println(tmp);
                urlvalido = true;
            }
        } while (!urlvalido);

        return String.valueOf(i);

    }

    public static void getreservas() {
        String url = "http://deti-tqs-vm6.ua.pt:8080/mavenViajanteTQS-1.0-SNAPSHOT/webresources/entities.reserva/fetch/" + String.valueOf(id);

        try {
            DocumentBuilderFactory f
                    = DocumentBuilderFactory.newInstance();
            DocumentBuilder b = f.newDocumentBuilder();
            Document doc = b.parse(url);

            doc.getDocumentElement().normalize();
            System.out.println("Root element: "
                    + doc.getDocumentElement().getNodeName());

            // loop through each item
            NodeList items = doc.getElementsByTagName("reserva");

            for (int i = 0; i < items.getLength(); i++) {
                Node n = items.item(i);
                if (n.getNodeType() != Node.ELEMENT_NODE) {
                    continue;
                }
                Element e = (Element) n;

                
                
                NodeList optionsNlc = doc.getElementsByTagName("idUtilizadorId");
                Element optionsElements = (Element) optionsNlc.item(i);

                NodeList optionNlc = optionsElements.getElementsByTagName("idUtilizadorId");
                Element titleElem2 = (Element) optionNlc.item(0);
                System.out.println(titleElem2);
                

                // get the "title elem" in this item (only one)
                NodeList titleList
                        = e.getElementsByTagName("utilizador");
                Element titleElem = (Element) titleList.item(0);
                System.out.println(titleElem);

                // get the "text node" in the title (only one)
                Node titleNode = titleElem.getChildNodes().item(0);
                utilizador = titleNode.getNodeValue();

                NodeList titleListpass
                        = e.getElementsByTagName("password");
                Element titleElempass = (Element) titleListpass.item(0);

                // get the "text node" in the title (only one)
                Node titleNodepass = titleElempass.getChildNodes().item(0);
                password = titleNodepass.getNodeValue();

                NodeList titleListcompanhia
                        = e.getElementsByTagName("companhia");
                Element titleElemcompanhia = (Element) titleListcompanhia.item(0);

                // get the "text node" in the title (only one)
                Node titleNodecompanhia = titleElemcompanhia.getChildNodes().item(0);
                companhia = titleNodecompanhia.getNodeValue();

                NodeList titleListid
                        = e.getElementsByTagName("id");
                Element titleElemid = (Element) titleListid.item(0);

                // get the "text node" in the title (only one)
                Node titleNodeid = titleElemid.getChildNodes().item(0);
                id = titleNodeid.getNodeValue();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
