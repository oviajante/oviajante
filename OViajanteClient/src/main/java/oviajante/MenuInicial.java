/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oviajante;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import static oviajante.ParseXMLDOM.companhia;
import static oviajante.ParseXMLDOM.id;
import static oviajante.ParseXMLDOM.password;
import static oviajante.ParseXMLDOM.utilizador;
import static sun.misc.RequestProcessor.postRequest;

/**
 *
 * @author rui
 */
public class MenuInicial {

    public static void main(String[] args) {

        boolean loginvalido = false;
        do {
            loginvalido = menuinial();
        } while (!loginvalido);

        menuInicial();

    }

    public static boolean menuinial() {

        JTextField username = new JTextField();
        JTextField password = new JPasswordField();
        Object[] message = {
            "Username:", username,
            "Password:", password
        };

        int option = JOptionPane.showConfirmDialog(null, message, "Login", JOptionPane.OK_CANCEL_OPTION);
        if (option == JOptionPane.OK_OPTION) {

            boolean loginvalido = ParseXMLDOM.parseAgenciaLogin(username.getText(), password.getText());

            return loginvalido;

        } else {
            System.out.println("Login canceled");
        }
        return false;
    }

    public static void menuInicial() {

        // MENU 
        List<String> optionList = new ArrayList<String>();
        optionList.add("1");
        optionList.add("2");
        optionList.add("3");
        Object[] options = optionList.toArray();
        int value = JOptionPane.showOptionDialog(
                null,
                "Seleciona uma opcao:\n 1. Criar Pacote \n"
                + " 2. Ver Reservas\n"
                + " 3. Logout \n",
                "Menu",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                options,
                optionList.get(0));

        int operacao = Integer.parseInt(optionList.get(value));

        if (operacao == 1) {

            try {
                inserirPacote();

            } catch (URISyntaxException ex) {
                Logger.getLogger(MenuInicial.class.getName()).log(Level.SEVERE, null, ex);
            } catch (HttpException ex) {
                Logger.getLogger(MenuInicial.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(MenuInicial.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (operacao == 2) {

            DefaultTableModel model = new DefaultTableModel();
            JTable table = new JTable(model);

            model.addColumn("ID");
            model.addColumn("Titulo");
            model.addColumn("nome");
            model.addColumn("email");
            model.addColumn("telemovel");

            String url = "http://deti-tqs-vm6.ua.pt:8080/mavenViajanteTQS-1.0-SNAPSHOT/webresources/entities.reserva/fetch/" + String.valueOf(ParseXMLDOM.id);

            try {
                DocumentBuilderFactory f
                        = DocumentBuilderFactory.newInstance();
                DocumentBuilder b = f.newDocumentBuilder();
                Document doc = b.parse(url);

                doc.getDocumentElement().normalize();
                System.out.println("Root element: "
                        + doc.getDocumentElement().getNodeName());

                // loop through each item
                NodeList items = doc.getElementsByTagName("reserva");

                for (int i = 0; i < items.getLength(); i++) {

                    System.out.println("ola");
                    Node n = items.item(i);
                    if (n.getNodeType() != Node.ELEMENT_NODE) {
                        continue;
                    }
                    Element e = (Element) n;

                    // get the "title elem" in this item (only one)
                    NodeList titleList
                            = e.getElementsByTagName("datapartida");
                    Element titleElem = (Element) titleList.item(0);

                    // get the "text node" in the title (only one)
                    Node titleNode = titleElem.getChildNodes().item(0);
                    String data = titleNode.getNodeValue();

                    NodeList titleListid
                            = e.getElementsByTagName("id");
                    Element titleElemid = (Element) titleListid.item(0);

                    // get the "text node" in the title (only one)
                    Node titleNodeid = titleElemid.getChildNodes().item(0);
                    String id = titleNodeid.getNodeValue();

                    NodeList email = e.getElementsByTagName("email");
                    Element elementEmail = (Element) email.item(0);

                    // get the "text node" in the title (only one)
                    Node titleEmail = elementEmail.getChildNodes().item(0);
                    String Emailasdasdasd = titleEmail.getNodeValue();

                    NodeList titulo = e.getElementsByTagName("titulo");
                    Element elementEmailtitulo = (Element) titulo.item(0);

                    // get the "text node" in the title (only one)
                    Node titleEmailtitulo = elementEmailtitulo.getChildNodes().item(0);
                    String Emailasdasdasdtitulo = titleEmailtitulo.getNodeValue();

                    NodeList titulonome = e.getElementsByTagName("nome");
                    Element elementEmailnome = (Element) titulonome.item(0);

                    // get the "text node" in the title (only one)
                    Node titleEmailnome = elementEmailnome.getChildNodes().item(0);
                    String Emailasdasdasdnome = titleEmailnome.getNodeValue();

                    NodeList titulonometelemovel = e.getElementsByTagName("telemovel");
                    Element elementEmailtelemovel = (Element) titulonometelemovel.item(0);

                    // get the "text node" in the title (only one)
                    Node titleEmailnometelemovel = elementEmailtelemovel.getChildNodes().item(0);
                    String Emailasdasdasdnometelemovel = titleEmailnometelemovel.getNodeValue();

                    model.addRow(new Object[]{id, Emailasdasdasdtitulo, Emailasdasdasdnome, Emailasdasdasd, Emailasdasdasdnometelemovel});
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            JFrame f = new JFrame();
            f.setSize(800, 600);
            f.add(new JScrollPane(table));
            f.setVisible(true);
        }
        if (operacao == 3) {
            System.exit(0);
        }

    }

    public static void inserirPacote() throws URISyntaxException, UnsupportedEncodingException, HttpException, IOException {
        JTextField Alojamento = new JTextField();
        JTextField Condicoes = new JTextField();
        JTextField Detalhe = new JTextField();
        JTextField ImagemURL = new JTextField();
        JTextField Itenerario = new JTextField();
        JTextField Preco = new JTextField();
        JTextField TipodePacote = new JTextField();
        JTextField Titulo = new JTextField();
        JTextField idAgenia = new JTextField();

        Object[] message = {
            "Titulo:", Titulo,
            "Alojamento:", Alojamento,
            "Condicoes:", Condicoes,
            "Detalhe:", Detalhe,
            "ImagemURL:", ImagemURL,
            "Itenerario:", Itenerario,
            "Preco:", Preco,
            "TipodePacote:", TipodePacote,};

        int option = JOptionPane.showConfirmDialog(null, message, "Login", JOptionPane.OK_CANCEL_OPTION);
        if (option == JOptionPane.OK_OPTION) {

            HttpPost postRequest = new HttpPost(
                    "http://deti-tqs-vm6.ua.pt:8080/mavenViajanteTQS-1.0-SNAPSHOT/webresources/entities.pacoteviagem");

            String content
                    = "<pacoteviagem>\n"
                    + "<alojamento>\n"
                    + Titulo.getText()
                    + "</alojamento>\n"
                    + "<condicoes>" + Condicoes.getText() + "</condicoes>\n"
                    + "<detalhe>" + Detalhe.getText() + "</detalhe>\n"
                    + "<id>" + ParseXMLDOM.checkemptyID() + "</id>\n"
                    + "<idAgenciaId>\n"
                    + "<companhia>" + ParseXMLDOM.companhia + "</companhia>\n"
                    + "<id>" + ParseXMLDOM.id + "</id>\n"
                    + "<password>" + ParseXMLDOM.password + "</password>\n"
                    + "<utilizador>" + ParseXMLDOM.utilizador + "</utilizador>\n"
                    + "</idAgenciaId>\n"
                    + "<imagemurl>\n"
                    + ImagemURL.getText()
                    + "</imagemurl>\n"
                    + "<itenerario>\n"
                    + Itenerario.getText()
                    + "</itenerario>\n"
                    + "<preco>" + Preco.getText() + "</preco>\n"
                    + "<tipopacote>promocoes</tipopacote>\n"
                    + "<titulo>\n"
                    + Titulo.getText()
                    + "</titulo>\n"
                    + "</pacoteviagem>";

            try {
                StringEntity entity = new StringEntity(content);
                entity.setContentType(new BasicHeader("Content-Type",
                        "application/xml"));
                postRequest.setEntity(entity);

                HttpClient httpclient = new DefaultHttpClient();
                HttpResponse response = httpclient.execute(postRequest);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
            System.out.println("login failed");
        }
    }

}
