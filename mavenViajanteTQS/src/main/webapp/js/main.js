﻿$(main)

function main() {
    $("#myNavbar").load("templates/geral/menu.html #menuPrincipal, #menuDireito", processLink);
    $("#maincontainer").load("templates/geral/menu.html #pesquisarPacote", processLink);
    initMenus();
}

// var globais
var session = null;
var utilizador = null;


/*MENU*/
function processLink() {

    $("#uiMenu").html(session);

    initBlogs();

    /*Travel Advisor e Home*/
    $("#homeMenu").click(function () {
        $("#maincontainer").load("templates/geral/menu.html #pesquisarPacote");
    });

    $("#travelAdvisor").click(function () {
        $("#maincontainer").load("templates/geral/menu.html #pesquisarPacote");
    });
    /*-------------------*/



    $("#a2login").click(function () {
       // $("#maincontainer").load("templates/login.html", enter);
       
       document.location.href = "templates/login.xhtml";
    });

    $("#a2registo").click(function () {
        $("#maincontainer").load("templates/registo.html");
    });

    $("#partilharViagem").click(function () {
        $("#maincontainer").load("templates/partilharViagem.html");
    });

    $("#quemSomos").click(function () {
        $("#maincontainer").load("templates/quemSomos.html")
    });

    $("#adicionarPacote").click(function () {
        $("#maincontainer").load("templates/adicionarPacote.html")
    });
    $("#verreservas").click(function () {
        $("#maincontainer").load("templates/consultarReserva.html")
    });
    


  
    $("#pacPromocoes").click(function () {
        $("#maincontainer").load("templates/pacotes/promocoes.html")
    });

    $("#pacCruzeiros").click(function () {
        $("#maincontainer").load("templates/pacotes/cruzeiros.html")
    });

    $("#pacDisneyland").click(function () {
        $("#maincontainer").load("templates/pacotes/disneyland.html")
    });

    $("#pacPraiasIlhas").click(function () {
        $("#maincontainer").load("templates/pacotes/praiasIlhas.html")
    });

    $("#pacEscapadinhas").click(function () {
        $("#maincontainer").load("templates/pacotes/escapadinhas.html")
    });

    $("#pacGrandesViagens").click(function () {
        $("#maincontainer").load("templates/pacotes/grandesViagens.html")
    });

}

/*LOGIN*/
function login() {
    if (document.getElementById("ucomum").selected) {
        loginUtilizador();
    }
    else if (document.getElementById("uagencia").selected) {
        loginAgencia();
    }

}

function loginUtilizador() {
    $.ajax({
        dataType: "json",
        type: "GET",
        url: "data/users.json",
        success: checkLoginUtilizador,
        unsuccess: errorLog
    });
}

function loginAgencia() {
    $.ajax({
        dataType: "json",
        type: "GET",
        url: "data/userAgencia.json",
        success: checkLoginAgencia,
        unsuccess: errorLog
    });
}

function errorLog() {
    alert("error");
}

function checkLoginUtilizador(result, status, chr) {
    var user = $("#user").val();
    var pass = $("#pass").val();
    $.each(result.u, function (i, obj) {
        if (obj.username == user && obj.password == pass) {
            session = user;
            $("#myNavbar").load("templates/geral/menu.html #menuUI, #menuDireitoUI", processLink);
            $("#maincontainer").load("templates/geral/menu.html #pesquisarPacote");
        }
        else {
            $("#loginAlert").html("<p>Utilizador ou password incorreta!</p>");
        }

    });


}

function checkLoginAgencia(result, status, chr) {
    var user = $("#user").val();
    var pass = $("#pass").val();
   
        $.each(result.uAgencia, function (i, obj) {
            if (obj.username == user && obj.password == pass) {
                session = user;
                $("#myNavbar").load("templates/geral/menu.html #menuAgencia, #menuDireitoAgencia", processLink);
                $("#maincontainer").load("templates/geral/menu.html #pesquisarPacote");
            }
            else {
                $("#loginAlert").html("<p>Utilizador ou password incorreta!</p>");
            }

        });
    


}

function logout() {
    session = null;
    $("#myNavbar").load("templates/geral/menu.html #menuPrincipal, #menuDireito", processLink);
    $("#maincontainer").load("templates/geral/menu.html #pesquisarPacote");

}

/*REGISTAR*/
function registo() {
    ///*Guarda informacao de registo*/
    //var name = $("#nome").val();
    //var phone = $("#numTel").val();
    //var mail = $("#mail").val();
    //var nUtilizador = $("#nameUI").val();
    //var password = $("#passwordUI").val();

    var nome = document.getElementById("#nome");
    var tel = document.getElementById("#numTel");
    var mail = document.getElementById("#mail");
    var nomeU = document.getElementById("#nameUI");
    var passU = document.getElementById("#passwordUI");

    //Validacoes
    var filter = /^[0-9-+]+$/;
    $('#registoForm').validate({ // initialize the plugin
        rules: {
            name: {
                name: true
            },
            phone: {
                required: true,
                filter: true
            },
            mail: {
                required: true,
                email: true
            },
            nUtilizador: {
                required: true,
                minlength: 3
            }
        }
    });
    $("#maincontainer").load("templates/login.html");
}

/*AGENCIA */
function adicionarPacotes() {
    $('#adicionarPacote').load("templates/adicionarPacote.html");
}
/*INFORMACAO PACOTES*/
var pacote;
function infoPacotes(pac) {
    pacote = pac;
    $("#maincontainer").load("templates/pacotes/descriptPac/infoPAC.html", LoadINFO);
}

function LoadINFO() {
    $.ajax({
        url: "data/informacaoPacotes.json",
        type: "GET",
        dataType: "json",
        success: processINFO,
        error: processErrorINFO
    });
}

function processINFO(result, status, xhr) {

    if (pacote === 'mauricias') {
        var alojamentoT = result.pacotes.promocoes.mauricias.titulo; $("#alojTitulo").html(alojamentoT);
        var alojamentoText = result.pacotes.promocoes.mauricias.alojamento; $("#alojTexto").html(alojamentoText);
        var itenerario = result.pacotes.promocoes.mauricias.itenerario;
        $("#menu1").html("<h1>" + itenerario.dia1 + "</h1>" +
                                "<h3>" + itenerario.texto1 + "</h3>" +
                                "<h1>" + itenerario.dia2 + "</h1>" +
                                "<h3>" + itenerario.texto2 + "</h3>" +
                                "<h1>" + itenerario.dia3 + "</h1>" +
                                "<h3>" + itenerario.texto3 + "</h3>" +
                                "<h1>" + itenerario.dia4 + "</h1>" +
                                "<h3>" + itenerario.texto4 + "</h3>" +
                                "<h1>" + itenerario.dia5 + "</h1>" +
                                "<h3>" + itenerario.texto5 + "</h3>" +
                                "<h1>" + itenerario.dia6 + "</h1>" +
                                "<h3>" + itenerario.texto6 + "</h3>" +
                                "<h1>" + itenerario.dia7 + "</h1>" +
                                "<h3>" + itenerario.texto7 + "</h3>" +
                                "<h1>" + itenerario.dia8 + "</h1>" +
                                "<h3>" + itenerario.texto8 + "</h3>" +
                                "<h1>" + itenerario.dia9 + "</h1>" +
                                "<h3>" + itenerario.texto9 + "</h3>" +
                                "<h1>" + itenerario.dia10 + "</h1>" +
                                "<h3>" + itenerario.texto10 + "</h3>");

        var condicoes = result.pacotes.promocoes.mauricias;
        $("#menu2").html("<h1> Condições/Serviços Incluídos</h1>" +
                                 "<h3>" + condicoes.condicoesI + "</h3>" +
                                 "<h1> Condições/Serviços Excluídos</h1>" +
                                 "<h3>" + condicoes.condicoesE + "</h3>");
    }
    else if (pacote === 'portoSeguro') {
        var alojamentoT = result.pacotes.promocoes.portoSeguro.titulo; $("#alojTitulo").html(alojamentoT);
        var alojamentoText = result.pacotes.promocoes.portoSeguro.alojamento; $("#alojTexto").html(alojamentoText);
        var itenerario = result.pacotes.promocoes.portoSeguro.itenerario;
        $("#menu1").html("<h1>" + itenerario.dia1 + "</h1>" +
                                "<h3>" + itenerario.texto1 + "</h3>" +
                                "<h1>" + itenerario.dia2 + "</h1>" +
                                "<h3>" + itenerario.texto2 + "</h3>" +
                                "<h1>" + itenerario.dia3 + "</h1>" +
                                "<h3>" + itenerario.texto3 + "</h3>" +
                                "<h1>" + itenerario.dia4 + "</h1>" +
                                "<h3>" + itenerario.texto4 + "</h3>");

        var condicoes = result.pacotes.promocoes.portoSeguro;
        $("#menu2").html("<h1> Condições/Serviços Incluídos</h1>" +
                                 "<h3>" + condicoes.condicoesI + "</h3>" +
                                 "<h1> Condições/Serviços Excluídos</h1>" +
                                 "<h3>" + condicoes.condicoesE + "</h3>");
    }
    else if (pacote === 'agadir') {
        var alojamentoT = result.pacotes.promocoes.agadir.titulo; $("#alojTitulo").html(alojamentoT);
        var alojamentoText = result.pacotes.promocoes.agadir.alojamento; $("#alojTexto").html(alojamentoText);
        var itenerario = result.pacotes.promocoes.agadir.itenerario;
        $("#menu1").html("<h1>" + itenerario.dia1 + "</h1>" +
                                "<h3>" + itenerario.texto1 + "</h3>" +
                                "<h1>" + itenerario.dia2 + "</h1>" +
                                "<h3>" + itenerario.texto2 + "</h3>" +
                                "<h1>" + itenerario.dia3 + "</h1>" +
                                "<h3>" + itenerario.texto3 + "</h3>" +
                                "<h1>" + itenerario.dia4 + "</h1>" +
                                "<h3>" + itenerario.texto4 + "</h3>");

        var condicoes = result.pacotes.promocoes.agadir;
        $("#menu2").html("<h1> Condições/Serviços Incluídos</h1>" +
                                 "<h3>" + condicoes.condicoesI + "</h3>" +
                                 "<h1> Condições/Serviços Excluídos</h1>" +
                                 "<h3>" + condicoes.condicoesE + "</h3>");
    }
    else if (pacote === 'algarve') {
        var alojamentoT = result.pacotes.promocoes.algarve.titulo; $("#alojTitulo").html(alojamentoT);
        var alojamentoText = result.pacotes.promocoes.algarve.alojamento; $("#alojTexto").html(alojamentoText);
        var itenerario = result.pacotes.promocoes.algarve.itenerario;
        $("#menu1").html("<h1>" + itenerario.dia1 + "</h1>" +
                                "<h3>" + itenerario.texto1 + "</h3>" +
                                "<h1>" + itenerario.dia2 + "</h1>" +
                                "<h3>" + itenerario.texto2 + "</h3>" +
                                "<h1>" + itenerario.dia3 + "</h1>" +
                                "<h3>" + itenerario.texto3 + "</h3>");

        var condicoes = result.pacotes.promocoes.algarve;
        $("#menu2").html("<h1> Condições/Serviços Incluídos</h1>" +
                                 "<h3>" + condicoes.condicoesI + "</h3>" +
                                 "<h1> Condições/Serviços Excluídos</h1>" +
                                 "<h3>" + condicoes.condicoesE + "</h3>");
    }
    else if (pacote === 'caraibas') {
        var alojamentoT = result.pacotes.promocoes.caraibas.titulo; $("#alojTitulo").html(alojamentoT);
        var alojamentoText = result.pacotes.promocoes.caraibas.alojamento; $("#alojTexto").html(alojamentoText);
        var itenerario = result.pacotes.promocoes.caraibas.itenerario;
        $("#menu1").html("<h1>" + itenerario.dia1 + "</h1>" +
                                "<h3>" + itenerario.texto1 + "</h3>" +
                                "<h1>" + itenerario.dia2 + "</h1>" +
                                "<h3>" + itenerario.texto2 + "</h3>" +
                                "<h1>" + itenerario.dia3 + "</h1>" +
                                "<h3>" + itenerario.texto3 + "</h3>" +
                                "<h1>" + itenerario.dia4 + "</h1>" +
                                "<h3>" + itenerario.texto4 + "</h3>");

        var condicoes = result.pacotes.promocoes.caraibas;
        $("#menu2").html("<h1> Condições/Serviços Incluídos</h1>" +
                                 "<h3>" + condicoes.condicoesI + "</h3>" +
                                 "<h1> Condições/Serviços Excluídos</h1>" +
                                 "<h3>" + condicoes.condicoesE + "</h3>");
    }
    else if (pacote === 'montenegro') {
        var alojamentoT = result.pacotes.promocoes.montenegro.titulo; $("#alojTitulo").html(alojamentoT);
        var alojamentoText = result.pacotes.promocoes.montenegro.alojamento; $("#alojTexto").html(alojamentoText);
        var itenerario = result.pacotes.promocoes.montenegro.itenerario;
        $("#menu1").html("<h1>" + itenerario.dia1 + "</h1>" +
                                "<h3>" + itenerario.texto1 + "</h3>" +
                                "<h1>" + itenerario.dia2 + "</h1>" +
                                "<h3>" + itenerario.texto2 + "</h3>" +
                                "<h1>" + itenerario.dia3 + "</h1>" +
                                "<h3>" + itenerario.texto3 + "</h3>");

        var condicoes = result.pacotes.promocoes.montenegro;
        $("#menu2").html("<h1> Condições/Serviços Incluídos</h1>" +
                                 "<h3>" + condicoes.condicoesI + "</h3>" +
                                 "<h1> Condições/Serviços Excluídos</h1>" +
                                 "<h3>" + condicoes.condicoesE + "</h3>");
    }
    else if (pacote === 'cruzCaraibas') {
        var alojamentoText = result.pacotes.cruzeiros.caraibas.alojamento; $("#alojTexto").html(alojamentoText);
        var itenerario = result.pacotes.cruzeiros.caraibas.itenerario;
        $("#menu1").html("<h1>" + itenerario.dia1 + "</h1>" +
                                "<h3>" + itenerario.texto1 + "</h3>" +
                                "<h1>" + itenerario.dia2 + "</h1>" +
                                "<h1>" + itenerario.dia3 + "</h1>" +
                                "<h3>" + itenerario.texto3 + "</h3>" +
                                "<h1>" + itenerario.dia4 + "</h1>" +
                                "<h3>" + itenerario.texto4 + "</h3>" +
                                "<h1>" + itenerario.dia5 + "</h1>" +
                                "<h1>" + itenerario.dia6 + "</h1>" +
                                "<h3>" + itenerario.texto6 + "</h3>" +
                                "<h1>" + itenerario.dia7 + "</h1>" +
                                "<h3>" + itenerario.texto7 + "</h3>");

        var condicoes = result.pacotes.cruzeiros.caraibas;
        $("#menu2").html("<h1> Condições Incluídas</h1>" +
                                 "<h3>" + condicoes.condicoes + "</h3>");
    }
    else if (pacote === 'cruzNoruega') {
        var alojamentoText = result.pacotes.cruzeiros.noruega.alojamento; $("#alojTexto").html(alojamentoText);
        var itenerario = result.pacotes.cruzeiros.noruega.itenerario;
        $("#menu1").html("<h1>" + itenerario.dia1 + "</h1>" +
                                "<h3>" + itenerario.texto1 + "</h3>" +
                                "<h1>" + itenerario.dia2 + "</h1>" +
                                "<h3>" + itenerario.texto2 + "</h3>" +
                                "<h1>" + itenerario.dia3 + "</h1>" +
                                "<h1>" + itenerario.dia4 + "</h1>" +
                                "<h3>" + itenerario.texto4 + "</h3>" +
                                "<h1>" + itenerario.dia5 + "</h1>" +
                                "<h1>" + itenerario.dia6 + "</h1>" +
                                "<h3>" + itenerario.texto6 + "</h3>" +
                                "<h1>" + itenerario.dia7 + "</h1>" +
                                "<h1>" + itenerario.dia8 + "</h1>");

        var condicoes = result.pacotes.cruzeiros.noruega.condicoes;
        $("#menu2").html("<h1> Condições Incluídas</h1>" +
                                 "<h3>" + condicoes + "</h3>");
    }
    else if (pacote === 'cruzDisney') {
        var alojamentoText = result.pacotes.cruzeiros.disney.alojamento; $("#alojTexto").html(alojamentoText);
        var itenerario = result.pacotes.cruzeiros.disney.itenerario;
        $("#menu1").html("<h2>" + itenerario + "</h2>");

        var condicoes = result.pacotes.cruzeiros.disney.condicoes;
        $("#menu2").html("<h1> Condições Incluídas</h1>" +
                                 "<h3>" + condicoes + "</h3>");
    }
    else if (pacote === 'cruzGrecia') {
        var alojamentoText = result.pacotes.cruzeiros.grecia.alojamento; $("#alojTexto").html(alojamentoText);
        var itenerario = result.pacotes.cruzeiros.grecia.itenerario;
        $("#menu1").html("<h1>" + itenerario.dia1 + "</h1>" +
                                "<h3>" + itenerario.texto1 + "</h3>" +
                                "<h1>" + itenerario.dia2 + "</h1>" +
                                "<h1>" + itenerario.dia3 + "</h1>" +
                                "<h3>" + itenerario.texto3 + "</h3>" +
                                "<h1>" + itenerario.dia4 + "</h1>" +
                                "<h1>" + itenerario.dia5 + "</h1>" +
                                "<h3>" + itenerario.texto5 + "</h3>" +
                                "<h1>" + itenerario.dia6 + "</h1>" +
                                "<h3>" + itenerario.texto6 + "</h3>" +
                                "<h1>" + itenerario.dia7 + "</h1>" +
                                "<h3>" + itenerario.texto7 + "</h3>" +
                                "<h1>" + itenerario.dia8 + "</h1>" +
                                "<h1>" + itenerario.dia9 + "</h1>" +
                                "<h3>" + itenerario.texto9 + "</h3>" +
                                "<h1>" + itenerario.dia10 + "</h1>" +
                                "<h3>" + itenerario.dia11 + "</h3>");

        var condicoes = result.pacotes.cruzeiros.grecia.condicoes;
        $("#menu2").html("<h1> Condições Incluídas</h1>" +
                                 "<h3>" + condicoes + "</h3>");
    }
    else if (pacote === 'cruzVeneza') {
        var alojamentoText = result.pacotes.cruzeiros.veneza.alojamento; $("#alojTexto").html(alojamentoText);
        var itenerario = result.pacotes.cruzeiros.veneza.itenerario;
        $("#menu1").html("<h1>" + itenerario.dia1 + "</h1>" +
            "<h1>" + itenerario.dia3 + "</h1>" +
            "<h1>" + itenerario.dia4 + "</h1>" +
            "<h1>" + itenerario.dia5 + "</h1>" +
            "<h1>" + itenerario.dia6 + "</h1>" +
            "<h1>" + itenerario.dia7 + "</h1>" +
            "<h1>" + itenerario.dia8 + "</h1>");

        var condicoes = result.pacotes.cruzeiros.veneza.condicoes;
        $("#menu2").html("<h1> Condições Incluídas</h1>" +
                                 "<h3>" + condicoes + "</h3>");
    }
    else if (pacote === 'disneyParis') {
        $("#alojTexto").html(result.pacotes.disney.disneyParis.alojamento);
        $("#menu1").html("<h2>" + result.pacotes.disney.disneyParis.itenerario + "</h2>");
        $("#menu2").html("<h1> Condições Incluídas</h1>" +
                                 "<h3>" + result.pacotes.disney.disneyParis.condicoes + "</h3>");
    }
    else if (pacote === 'disneyPrimavera') {
        var alojamentoT = result.pacotes.disney.disneyPrimavera.titulo; $("#alojTitulo").html(alojamentoT);
        var alojamentoText = result.pacotes.disney.disneyPrimavera.alojamento; $("#alojTexto").html(alojamentoText);
        var itenerario = result.pacotes.disney.disneyPrimavera.itenerario;
        $("#menu1").html("<h1>" + itenerario.dia1 + "</h1>" +
                                "<h3>" + itenerario.texto1 + "</h3>" +
                                "<h1>" + itenerario.dia2 + "</h1>" +
                                "<h3>" + itenerario.texto2 + "</h3>" +
                                "<h1>" + itenerario.dia3 + "</h1>" +
                                "<h3>" + itenerario.texto3 + "</h3>");

        var condicoes = result.pacotes.disney.disneyPrimavera.condicoes;
        $("#menu2").html("<h1> Condições/Serviços Incluídos</h1>" +
                                 "<h3>" + condicoes.condicoesI + "</h3>" +
                                 "<h1> Condições/Serviços Excluídos</h1>" +
                                 "<h3>" + condicoes.condicoesE + "</h3>");
    }
    else if (pacote === 'disneyInverno') {
        $("#alojTexto").html(result.pacotes.disney.disneyInverno.alojamento);
        $("#menu1").html("<h2>" + result.pacotes.disney.disneyInverno.itenerario + "</h2>");
        $("#menu2").html("<h1> Condições Incluídas</h1>" +
                                 "<h3>" + result.pacotes.disney.disneyInverno.condicoes + "</h3>");
    }
    else if (pacote === 'disneyVerao') {
        var alojamentoT = result.pacotes.disney.disneyVerao.titulo; $("#alojTitulo").html(alojamentoT);
        var alojamentoText = result.pacotes.disney.disneyVerao.alojamento; $("#alojTexto").html(alojamentoText);
        var itenerario = result.pacotes.disney.disneyVerao.itenerario;
        $("#menu1").html("<h1>" + itenerario.dia1 + "</h1>" +
                                "<h3>" + itenerario.texto1 + "</h3>" +
                                "<h1>" + itenerario.dia2 + "</h1>" +
                                "<h3>" + itenerario.texto2 + "</h3>" +
                                "<h1>" + itenerario.dia3 + "</h1>" +
                                "<h3>" + itenerario.texto3 + "</h3>");

        var condicoes = result.pacotes.disney.disneyVerao.condicoes;
        $("#menu2").html("<h1> Condições/Serviços Incluídos</h1>" +
                                 "<h3>" + condicoes.condicoesI + "</h3>" +
                                 "<h1> Condições/Serviços Excluídos</h1>" +
                                 "<h3>" + condicoes.condicoesE + "</h3>");
    }
    else if (pacote === 'escapadinhaBeiraMar') {
        var alojamentoT = result.pacotes.escapadinhas.beiraMar.titulo; $("#alojTitulo").html(alojamentoT);
        var alojamentoText = result.pacotes.escapadinhas.beiraMar.alojamento; $("#alojTexto").html(alojamentoText);
        var itenerario = result.pacotes.escapadinhas.beiraMar.itenerario;
        $("#menu1").html("<h1>" + itenerario.dia1 + "</h1>" +
                                "<h3>" + itenerario.texto1 + "</h3>" +
                                "<h1>" + itenerario.dia2 + "</h1>" +
                                "<h3>" + itenerario.texto2 + "</h3>");

        var condicoes = result.pacotes.escapadinhas.beiraMar.condicoes;
        $("#menu2").html("<h1> Condições/Serviços Incluídos</h1>" +
                                 "<h3>" + condicoes.condicoesI + "</h3>" +
                                 "<h1> Condições/Serviços Excluídos</h1>" +
                                 "<h3>" + condicoes.condicoesE + "</h3>");
    }
    else if (pacote === 'escapadinhaRomance') {
        var alojamentoT = result.pacotes.escapadinhas.romance.titulo; $("#alojTitulo").html(alojamentoT);
        var alojamentoText = result.pacotes.escapadinhas.romance.alojamento; $("#alojTexto").html(alojamentoText);
        var itenerario = result.pacotes.escapadinhas.romance.itenerario;
        $("#menu1").html("<h1>" + itenerario.dia1 + "</h1>" +
                                "<h3>" + itenerario.texto1 + "</h3>" +
                                "<h1>" + itenerario.dia2 + "</h1>" +
                                "<h3>" + itenerario.texto2 + "</h3>");

        var condicoes = result.pacotes.escapadinhas.romance.condicoes;
        $("#menu2").html("<h1> Condições/Serviços Incluídos</h1>" +
                                 "<h3>" + condicoes.condicoesI + "</h3>" +
                                 "<h1> Condições/Serviços Excluídos</h1>" +
                                 "<h3>" + condicoes.condicoesE + "</h3>");
    }
    else if (pacote === 'escapadinhaSabores') {
        var alojamentoT = result.pacotes.escapadinhas.sabores.titulo; $("#alojTitulo").html(alojamentoT);
        var alojamentoText = result.pacotes.escapadinhas.sabores.alojamento; $("#alojTexto").html(alojamentoText);
        var itenerario = result.pacotes.escapadinhas.sabores.itenerario;
        $("#menu1").html("<h1>" + itenerario.dia1 + "</h1>" +
                                "<h3>" + itenerario.texto1 + "</h3>" +
                                "<h1>" + itenerario.dia2 + "</h1>" +
                                "<h3>" + itenerario.texto2 + "</h3>");

        var condicoes = result.pacotes.escapadinhas.sabores.condicoes;
        $("#menu2").html("<h1> Condições/Serviços Incluídos</h1>" +
                                 "<h3>" + condicoes.condicoesI + "</h3>" +
                                 "<h1> Condições/Serviços Excluídos</h1>" +
                                 "<h3>" + condicoes.condicoesE + "</h3>");
    }
    else if (pacote === 'escapadinhaSerra') {
        var alojamentoT = result.pacotes.escapadinhas.serra.titulo; $("#alojTitulo").html(alojamentoT);
        var alojamentoText = result.pacotes.escapadinhas.serra.alojamento; $("#alojTexto").html(alojamentoText);
        var itenerario = result.pacotes.escapadinhas.serra.itenerario;
        $("#menu1").html("<h1>" + itenerario.dia1 + "</h1>" +
                                "<h3>" + itenerario.texto1 + "</h3>" +
                                "<h1>" + itenerario.dia2 + "</h1>" +
                                "<h3>" + itenerario.texto2 + "</h3>" +
                                "<h1>" + itenerario.dia3 + "</h1>" +
                                "<h3>" + itenerario.texto3 + "</h3>");

        var condicoes = result.pacotes.escapadinhas.serra.condicoes;
        $("#menu2").html("<h1> Condições/Serviços Incluídos</h1>" +
                                 "<h3>" + condicoes.condicoesI + "</h3>" +
                                 "<h1> Condições/Serviços Excluídos</h1>" +
                                 "<h3>" + condicoes.condicoesE + "</h3>");
    }
    else if (pacote === 'escapadinhaSPA') {
        var alojamentoT = result.pacotes.escapadinhas.spa.titulo; $("#alojTitulo").html(alojamentoT);
        var alojamentoText = result.pacotes.escapadinhas.spa.alojamento; $("#alojTexto").html(alojamentoText);
        var itenerario = result.pacotes.escapadinhas.spa.itenerario;
        $("#menu1").html("<h1>" + itenerario.dia1 + "</h1>" +
                                "<h3>" + itenerario.texto1 + "</h3>" +
                                "<h1>" + itenerario.dia2 + "</h1>" +
                                "<h3>" + itenerario.texto2 + "</h3>" +
                                "<h1>" + itenerario.dia3 + "</h1>" +
                                "<h3>" + itenerario.texto3 + "</h3>");

        var condicoes = result.pacotes.escapadinhas.spa.condicoes;
        $("#menu2").html("<h1> Condições/Serviços Incluídos</h1>" +
                                 "<h3>" + condicoes.condicoesI + "</h3>" +
                                 "<h1> Condições/Serviços Excluídos</h1>" +
                                 "<h3>" + condicoes.condicoesE + "</h3>");
    }
    else if (pacote === 'gViagemMadagascar') {
        var alojamentoT = result.pacotes.grandesViagens.madagascar.titulo; $("#alojTitulo").html(alojamentoT);
        var alojamentoText = result.pacotes.grandesViagens.madagascar.alojamento; $("#alojTexto").html(alojamentoText);
        var itenerario = result.pacotes.grandesViagens.madagascar.itenerario;
        $("#menu1").html("<h1>" + itenerario.dia1 + "</h1>" +
                                "<h3>" + itenerario.texto1 + "</h3>" +
                                "<h1>" + itenerario.dia2 + "</h1>" +
                                "<h3>" + itenerario.texto2 + "</h3>" +
                                "<h1>" + itenerario.dia3 + "</h1>" +
                                "<h3>" + itenerario.texto3 + "</h3>" +
                                "<h1>" + itenerario.dia4 + "</h1>" +
                                "<h3>" + itenerario.texto4 + "</h3>" +
                                "<h1>" + itenerario.dia5 + "</h1>" +
                                "<h3>" + itenerario.texto5 + "</h3>" +
                                "<h1>" + itenerario.dia6 + "</h1>" +
                                "<h3>" + itenerario.texto6 + "</h3>" +
                                "<h1>" + itenerario.dia7 + "</h1>" +
                                "<h3>" + itenerario.texto7 + "</h3>" +
                                "<h1>" + itenerario.dia8 + "</h1>" +
                                "<h3>" + itenerario.texto8 + "</h3>" +
                                "<h1>" + itenerario.dia9 + "</h1>" +
                                "<h3>" + itenerario.texto9 + "</h3>" +
                                "<h1>" + itenerario.dia10 + "</h1>" +
                                "<h3>" + itenerario.texto10 + "</h3>" +
                                "<h1>" + itenerario.dia11 + "</h1>" +
                                "<h3>" + itenerario.texto11 + "</h3>" +
                                "<h1>" + itenerario.dia12 + "</h1>" +
                                "<h3>" + itenerario.texto12 + "</h3>" +
                                "<h1>" + itenerario.dia13 + "</h1>" +
                                "<h3>" + itenerario.texto13 + "</h3>" +
                                "<h1>" + itenerario.dia14 + "</h1>" +
                                "<h3>" + itenerario.texto14 + "</h3>");

        $("#menu2").html("<h1> Condições/Serviços Incluídos</h1>" +
                                 "<h3>" + result.pacotes.grandesViagens.madagascar.condicoesI + "</h3>" +
                                 "<h1> Condições/Serviços Excluídos</h1>" +
                                 "<h3>" + result.pacotes.grandesViagens.madagascar.condicoesE + "</h3>");
    }
    else if (pacote === 'gViagemAmericaCentral') {
        var alojamentoT = result.pacotes.grandesViagens.americaCentral.titulo; $("#alojTitulo").html(alojamentoT);
        var alojamentoText = result.pacotes.grandesViagens.americaCentral.alojamento; $("#alojTexto").html(alojamentoText);
        var itenerario = result.pacotes.grandesViagens.americaCentral.itenerario;
        $("#menu1").html("<h1>" + itenerario.dia1 + "</h1>" +
                                "<h3>" + itenerario.texto1 + "</h3>" +
                                "<h1>" + itenerario.dia2 + "</h1>" +
                                "<h3>" + itenerario.texto2 + "</h3>" +
                                "<h1>" + itenerario.dia3 + "</h1>" +
                                "<h3>" + itenerario.texto3 + "</h3>" +
                                "<h1>" + itenerario.dia4 + "</h1>" +
                                "<h3>" + itenerario.texto4 + "</h3>" +
                                "<h1>" + itenerario.dia5 + "</h1>" +
                                "<h3>" + itenerario.texto5 + "</h3>" +
                                "<h1>" + itenerario.dia6 + "</h1>" +
                                "<h3>" + itenerario.texto6 + "</h3>" +
                                "<h1>" + itenerario.dia7 + "</h1>" +
                                "<h3>" + itenerario.texto7 + "</h3>" +
                                "<h1>" + itenerario.dia8 + "</h1>" +
                                "<h3>" + itenerario.texto8 + "</h3>" +
                                "<h1>" + itenerario.dia9 + "</h1>" +
                                "<h3>" + itenerario.texto9 + "</h3>");
        ;
        $("#menu2").html("<h1> Condições/Serviços Incluídos</h1>" +
                                 "<h3>" + result.pacotes.grandesViagens.americaCentral.condicoesI + "</h3>" +
                                 "<h1> Condições/Serviços Excluídos</h1>" +
                                 "<h3>" + result.pacotes.grandesViagens.americaCentral.condicoesE + "</h3>");

    }
    else if (pacote === 'gViagemAmericaNorte') {
        var alojamentoT = result.pacotes.grandesViagens.americaNorte.titulo; $("#alojTitulo").html(alojamentoT);
        var alojamentoText = result.pacotes.grandesViagens.americaNorte.alojamento; $("#alojTexto").html(alojamentoText);
        var itenerario = result.pacotes.grandesViagens.americaNorte.itenerario;
        $("#menu1").html("<h1>" + itenerario.dia1 + "</h1>" +
                                "<h3>" + itenerario.texto1 + "</h3>" +
                                "<h1>" + itenerario.dia2 + "</h1>" +
                                "<h3>" + itenerario.texto2 + "</h3>" +
                                "<h1>" + itenerario.dia3 + "</h1>" +
                                "<h3>" + itenerario.texto3 + "</h3>" +
                                "<h1>" + itenerario.dia4 + "</h1>" +
                                "<h3>" + itenerario.texto4 + "</h3>");

        $("#menu2").html("<h1> Condições/Serviços Incluídos</h1>" +
                                 "<h3>" + result.pacotes.grandesViagens.americaNorte.condicoesI + "</h3>" +
                                 "<h1> Condições/Serviços Excluídos</h1>" +
                                 "<h3>" + result.pacotes.grandesViagens.americaNorte.condicoesE + "</h3>");

    }
    else if (pacote === 'gViagemAmericaSul') {
        var alojamentoT = result.pacotes.grandesViagens.americaSul.titulo; $("#alojTitulo").html(alojamentoT);
        var alojamentoText = result.pacotes.grandesViagens.americaSul.alojamento; $("#alojTexto").html(alojamentoText);
        var itenerario = result.pacotes.grandesViagens.americaSul.itenerario;
        $("#menu1").html("<h1>" + itenerario.dia1 + "</h1>" +
                                "<h3>" + itenerario.texto1 + "</h3>" +
                                "<h1>" + itenerario.dia2 + "</h1>" +
                                "<h3>" + itenerario.texto2 + "</h3>" +
                                "<h1>" + itenerario.dia3 + "</h1>" +
                                "<h3>" + itenerario.texto3 + "</h3>" +
                                "<h1>" + itenerario.dia4 + "</h1>" +
                                "<h3>" + itenerario.texto4 + "</h3>" +
                                "<h1>" + itenerario.dia5 + "</h1>" +
                                "<h3>" + itenerario.texto5 + "</h3>" +
                                "<h1>" + itenerario.dia6 + "</h1>" +
                                "<h3>" + itenerario.texto6 + "</h3>" +
                                "<h1>" + itenerario.dia7 + "</h1>" +
                                "<h3>" + itenerario.texto7 + "</h3>" +
                                "<h1>" + itenerario.dia8 + "</h1>" +
                                "<h3>" + itenerario.texto8 + "</h3>");

        $("#menu2").html("<h1> Condições/Serviços Incluídos</h1>" +
                                 "<h3>" + result.pacotes.grandesViagens.americaSul.condicoesI + "</h3>" +
                                 "<h1> Condições/Serviços Excluídos</h1>" +
                                 "<h3>" + result.pacotes.grandesViagens.americaSul.condicoesE + "</h3>");

    }
    else if (pacote === 'gViagemAsia') {
        var alojamentoT = result.pacotes.grandesViagens.asia.titulo; $("#alojTitulo").html(alojamentoT);
        var alojamentoText = result.pacotes.grandesViagens.asia.alojamento; $("#alojTexto").html(alojamentoText);
        var itenerario = result.pacotes.grandesViagens.asia.itenerario;
        $("#menu1").html("<h1>" + itenerario.dia1 + "</h1>" +
                                "<h3>" + itenerario.texto1 + "</h3>" +
                                "<h1>" + itenerario.dia2 + "</h1>" +
                                "<h3>" + itenerario.texto2 + "</h3>" +
                                "<h1>" + itenerario.dia3 + "</h1>" +
                                "<h3>" + itenerario.texto3 + "</h3>" +
                                "<h1>" + itenerario.dia4 + "</h1>" +
                                "<h3>" + itenerario.texto4 + "</h3>" +
                                "<h1>" + itenerario.dia5 + "</h1>" +
                                "<h3>" + itenerario.texto5 + "</h3>" +
                                "<h1>" + itenerario.dia6 + "</h1>" +
                                "<h3>" + itenerario.texto6 + "</h3>" +
                                "<h1>" + itenerario.dia7 + "</h1>" +
                                "<h3>" + itenerario.texto7 + "</h3>" +
                                "<h1>" + itenerario.dia8 + "</h1>" +
                                "<h3>" + itenerario.texto8 + "</h3>" +
                                "<h1>" + itenerario.dia9 + "</h1>" +
                                "<h3>" + itenerario.texto9 + "</h3>");

        $("#menu2").html("<h1> Condições/Serviços Incluídos</h1>" +
                                 "<h3>" + result.pacotes.grandesViagens.asia.condicoesI + "</h3>" +
                                 "<h1> Condições/Serviços Excluídos</h1>" +
                                 "<h3>" + result.pacotes.grandesViagens.asia.condicoesE + "</h3>");

    }
    else if (pacote === 'gViagemDubai') {
        var alojamentoT = result.pacotes.grandesViagens.dubai.titulo; $("#alojTitulo").html(alojamentoT);
        var alojamentoText = result.pacotes.grandesViagens.dubai.alojamento; $("#alojTexto").html(alojamentoText);
        var itenerario = result.pacotes.grandesViagens.dubai.itenerario;
        $("#menu1").html("<h1>" + itenerario.dia1 + "</h1>" +
                                "<h3>" + itenerario.texto1 + "</h3>" +
                                "<h1>" + itenerario.dia2 + "</h1>" +
                                "<h3>" + itenerario.texto2 + "</h3>" +
                                "<h1>" + itenerario.dia3 + "</h1>" +
                                "<h3>" + itenerario.texto3 + "</h3>");

        $("#menu2").html("<h1> Condições/Serviços Incluídos</h1>" +
                                 "<h3>" + result.pacotes.grandesViagens.dubai.condicoesI + "</h3>" +
                                 "<h1> Condições/Serviços Excluídos</h1>" +
                                 "<h3>" + result.pacotes.grandesViagens.dubai.condicoesE + "</h3>");
    }
    else if (pacote === 'gViagemNI') {
        var alojamentoT = result.pacotes.grandesViagens.novaiorque.titulo; $("#alojTitulo").html(alojamentoT);
        var alojamentoText = result.pacotes.grandesViagens.novaiorque.alojamento; $("#alojTexto").html(alojamentoText);
        var itenerario = result.pacotes.grandesViagens.novaiorque.itenerario;
        $("#menu1").html("<h1>" + itenerario.dia1 + "</h1>" +
                                "<h3>" + itenerario.texto1 + "</h3>" +
                                "<h1>" + itenerario.dia2 + "</h1>" +
                                "<h3>" + itenerario.texto2 + "</h3>" +
                                "<h1>" + itenerario.dia3 + "</h1>" +
                                "<h3>" + itenerario.texto3 + "</h3>" +
                                "<h1>" + itenerario.dia4 + "</h1>" +
                                "<h3>" + itenerario.texto4 + "</h3>");

        $("#menu2").html("<h1> Condições/Serviços Incluídos</h1>" +
                                 "<h3>" + result.pacotes.grandesViagens.novaiorqu.condicoesI + "</h3>" +
                                 "<h1> Condições/Serviços Excluídos</h1>" +
                                 "<h3>" + result.pacotes.grandesViagens.novaiorqu.condicoesE + "</h3>");

    }
    else if (pacote === 'gViagemOriente') {
        var alojamentoT = result.pacotes.grandesViagens.oriente.titulo; $("#alojTitulo").html(alojamentoT);
        var alojamentoText = result.pacotes.grandesViagens.oriente.alojamento; $("#alojTexto").html(alojamentoText);
        var itenerario = result.pacotes.grandesViagens.oriente.itenerario;
        $("#menu1").html("<h1>" + itenerario.dia1 + "</h1>" +
                                "<h3>" + itenerario.texto1 + "</h3>" +
                                "<h1>" + itenerario.dia2 + "</h1>" +
                                "<h3>" + itenerario.texto2 + "</h3>" +
                                "<h1>" + itenerario.dia3 + "</h1>" +
                                "<h3>" + itenerario.texto3 + "</h3>" +
                                "<h1>" + itenerario.dia4 + "</h1>" +
                                "<h3>" + itenerario.texto4 + "</h3>" +
                                "<h1>" + itenerario.dia5 + "</h1>" +
                                "<h3>" + itenerario.texto5 + "</h3>" +
                                "<h1>" + itenerario.dia6 + "</h1>" +
                                "<h3>" + itenerario.texto6 + "</h3>");

        $("#menu2").html("<h1> Condições/Serviços Incluídos</h1>" +
                                 "<h3>" + result.pacotes.grandesViagens.oriente.condicoesI + "</h3>" +
                                 "<h1> Condições/Serviços Excluídos</h1>" +
                                 "<h3>" + result.pacotes.grandesViagens.oriente.condicoesE + "</h3>");

    }
    else if (pacote === 'praiaSalvador') {
        var alojamentoT = result.pacotes.praias.salvador.titulo; $("#alojTitulo").html(alojamentoT);
        var alojamentoText = result.pacotes.praias.salvador.alojamento; $("#alojTexto").html(alojamentoText);
        var itenerario = result.pacotes.praias.salvador.itenerario;
        $("#menu1").html("<h1>" + itenerario.dia1 + "</h1>" +
                                "<h3>" + itenerario.texto1 + "</h3>" +
                                "<h1>" + itenerario.dia2 + "</h1>" +
                                "<h3>" + itenerario.texto2 + "</h3>" +
                                "<h1>" + itenerario.dia3 + "</h1>" +
                                "<h3>" + itenerario.texto3 + "</h3>" +
                                "<h1>" + itenerario.dia4 + "</h1>" +
                                "<h3>" + itenerario.texto4 + "</h3>");

        var condicoes = result.pacotes.praias.salvador.condicoes;
        $("#menu2").html("<h1> Condições/Serviços Incluídos</h1>" +
                                 "<h3>" + condicoes.condicoesI + "</h3>" +
                                 "<h1> Condições/Serviços Excluídos</h1>" +
                                 "<h3>" + condicoes.condicoesE + "</h3>");
    }
    else if (pacote === 'praiaCaboVerde') {
        var alojamentoT = result.pacotes.praias.caboVerde.titulo; $("#alojTitulo").html(alojamentoT);
        var alojamentoText = result.pacotes.praias.caboVerde.alojamento; $("#alojTexto").html(alojamentoText);
        var itenerario = result.pacotes.praias.caboVerde.itenerario;
        $("#menu1").html("<h1>" + itenerario.dia1 + "</h1>" +
                                "<h3>" + itenerario.texto1 + "</h3>" +
                                "<h1>" + itenerario.dia2 + "</h1>" +
                                "<h3>" + itenerario.texto2 + "</h3>" +
                                "<h1>" + itenerario.dia3 + "</h1>" +
                                "<h3>" + itenerario.texto3 + "</h3>");

        var condicoes = result.pacotes.praias.caboVerde.condicoes;
        $("#menu2").html("<h1> Condições/Serviços Incluídos</h1>" +
                                 "<h3>" + condicoes.condicoesI + "</h3>" +
                                 "<h1> Condições/Serviços Excluídos</h1>" +
                                 "<h3>" + condicoes.condicoesE + "</h3>");
    }
    else if (pacote === 'praiaCaraibas') {
        var alojamentoT = result.pacotes.praias.caraibas.titulo; $("#alojTitulo").html(alojamentoT);
        var alojamentoText = result.pacotes.praias.caraibas.alojamento; $("#alojTexto").html(alojamentoText);
        var itenerario = result.pacotes.praias.caraibas.itenerario;
        $("#menu1").html("<h1>" + itenerario.dia1 + "</h1>" +
                                "<h3>" + itenerario.texto1 + "</h3>" +
                                "<h1>" + itenerario.dia2 + "</h1>" +
                                "<h3>" + itenerario.texto2 + "</h3>" +
                                "<h1>" + itenerario.dia3 + "</h1>" +
                                "<h3>" + itenerario.texto3 + "</h3>" +
                                "<h1>" + itenerario.dia4 + "</h1>" +
                                "<h3>" + itenerario.texto4 + "</h3>");

        var condicoes = result.pacotes.praias.caraibas.condicoes;
        $("#menu2").html("<h1> Condições/Serviços Incluídos</h1>" +
                                 "<h3>" + condicoes.condicoesI + "</h3>" +
                                 "<h1> Condições/Serviços Excluídos</h1>" +
                                 "<h3>" + condicoes.condicoesE + "</h3>");
    }
    else if (pacote === 'praiaEspanha') {
        var alojamentoT = result.pacotes.praias.espanha.titulo; $("#alojTitulo").html(alojamentoT);
        var alojamentoText = result.pacotes.praias.espanha.alojamento; $("#alojTexto").html(alojamentoText);
        var itenerario = result.pacotes.praias.espanha.itenerario;
        $("#menu1").html("<h1>" + itenerario.dia1 + "</h1>" +
                                "<h3>" + itenerario.texto1 + "</h3>");

        var condicoes = result.pacotes.praias.espanha.condicoes;
        $("#menu2").html("<h1> Condições/Serviços Incluídos</h1>" +
                                 "<h3>" + condicoes.condicoesI + "</h3>" +
                                 "<h1> Condições/Serviços Excluídos</h1>" +
                                 "<h3>" + condicoes.condicoesE + "</h3>");
    }
    else if (pacote === 'praiaExótica') {
        var alojamentoT = result.pacotes.praias.exoticas.titulo; $("#alojTitulo").html(alojamentoT);
        var alojamentoText = result.pacotes.praias.exoticas.alojamento; $("#alojTexto").html(alojamentoText);
        var itenerario = result.pacotes.praias.exoticas.itenerario;
        $("#menu1").html("<h1>" + itenerario.dia1 + "</h1>" +
                                "<h3>" + itenerario.texto1 + "</h3>" +
                                "<h1>" + itenerario.dia2 + "</h1>" +
                                "<h3>" + itenerario.texto2 + "</h3>" +
                                "<h1>" + itenerario.dia3 + "</h1>" +
                                "<h3>" + itenerario.texto3 + "</h3>" +
                                "<h1>" + itenerario.dia4 + "</h1>" +
                                "<h3>" + itenerario.texto4 + "</h3>");

        var condicoes = result.pacotes.praias.exoticas.condicoes;
        $("#menu2").html("<h1> Condições/Serviços Incluídos</h1>" +
                                 "<h3>" + condicoes.condicoesI + "</h3>" +
                                 "<h1> Condições/Serviços Excluídos</h1>" +
                                 "<h3>" + condicoes.condicoesE + "</h3>");
    }
    else if (pacote === 'praiaSaoTome') {
        var alojamentoT = result.pacotes.praias.saoTome.titulo; $("#alojTitulo").html(alojamentoT);
        var alojamentoText = result.pacotes.praias.saoTome.alojamento; $("#alojTexto").html(alojamentoText);
        var itenerario = result.pacotes.praias.saoTome.itenerario;
        $("#menu1").html("<h1>" + itenerario.dia1 + "</h1>" +
                                "<h3>" + itenerario.texto1 + "</h3>" +
                                "<h1>" + itenerario.dia2 + "</h1>" +
                                "<h3>" + itenerario.texto2 + "</h3>" +
                                "<h1>" + itenerario.dia4 + "</h1>" +
                                "<h3>" + itenerario.texto4 + "</h3>");

        var condicoes = result.pacotes.praias.saoTome.condicoes;
        $("#menu2").html("<h1> Condições/Serviços Incluídos</h1>" +
                                 "<h3>" + condicoes.condicoesI + "</h3>" +
                                 "<h1> Condições/Serviços Excluídos</h1>" +
                                 "<h3>" + condicoes.condicoesE + "</h3>");
    }

    changetabs('alojamento');
}

/*EDITAR PERFIL*/
function infoUser() {
    $("#maincontainer").load("templates/geral/paginaUI.html", processHTMLUser);
}

function processHTMLUser() {
    loadPerfilUtilizador();
}

function loadPerfilUtilizador() {
    $.ajax({
        dataType: "json",
        type: "GET",
        url: "data/users.json",
        success: processPerfil,
        unsuccess: errorLog
    });
}

function processPerfil(result, status, xhr) {
    for (var i = 0; i < result.u.length; i++) {
        var n = result.u[i];
        if (session === n.username) {
            utilizador = n.nome;
            //alert("Nome: " + n.nome);
            $("#nomePerfil").val(utilizador); //escreve o nome do utilizador no campo que se encontra no html reservar
            $("#emailPerfil").val(n.email);
            $("#telefPerfil").val(n.telefone);
            $("#usernamePerfil").val(n.username);
            $("#passwordPerfil").val(n.password);
            $("#dNascPerfil").val(n.dataNascimento);
            $("#fotoPerfil").attr("src", "../../img/equipa/" + n.foto);
        }
    }
}

function editarUI() {
    $("#maincontainer").load("templates/geral/editarUI.html", processHTMLEditUser);
}

function processHTMLEditUser() {
    loadEditarPerfilUtilizador();
}

function loadEditarPerfilUtilizador() {
    $.ajax({
        dataType: "json",
        type: "GET",
        url: "data/users.json",
        success: processEditarPerfil,
        unsuccess: errorLog
    });
}

function processEditarPerfil(result, status, xhr) {
    for (var i = 0; i < result.u.length; i++) {
        var n = result.u[i];
        if (session === n.username) {
            utilizador = n.nome;
            //alert("Nome: " + n.nome);
            $("#nomePerfil").val(utilizador); //escreve o nome do utilizador no campo que se encontra no html reservar
            $("#emailPerfil").val(n.email);
            $("#telefPerfil").val(n.telefone);
            $("#usernamePerfil").val(n.username);
            $("#passwordPerfil").val(n.password);
            $("#dNascPerfil").val(n.dataNascimento);
            $("#fotoPerfil").attr("src", "../../img/equipa/" + n.foto);
        }
    }
}

//function pesquisa() {
//    var pacote = document.getElementById("#pacote").value;
//    var local = document.getElementById("#local").value;
//    if (promocao.match("#promocoes") && local.match("#america")) {
//        for(var i=0; i < )
//    }
//}

function reservar() {
    if (session == null) {
        $("#maincontainer").load("templates/login.html");
    }
    else {
        $("#maincontainer").load("templates/pacotes/descriptPac/reservar.html", processHtmlReservar);
    }
}

function processHtmlReservar() {
    loadReservar();
}

function loadReservar() {
    $.ajax({
        dataType: "json",
        type: "GET",
        url: "data/users.json",
        success: processReserva,
        unsuccess: errorLog
    });
}

function processReserva(result, status, xhr) {
    for (var i = 0; i < result.u.length; i++) {
        var n = result.u[i];
        if (session === n.username) {
            utilizador = n.nome;
            //alert("Nome: " + n.nome);
            $("#usrReservar").val(utilizador); //escreve o nome do utilizador no campo que se encontra no html reservar
            $("#usremail").val(n.email);
            $("#usrtelef").val(n.telefone);

        }
    }
}

function reservarPacote() {
    var num = Math.random();
    $("#reservaAlert").html("<p>RESERVA EFETUADA COM SUCESSO</p><br>Número da reserva: " + num);

}

function changetabs(id) {
    if (id === 'alojamento') {
        $('#alojamento').css("visibility", "visible");
        $('#itenerario').css("visibility", "hidden");
        $('#condicoes').css("visibility", "hidden");
    }
    if (id === 'itenerario') {
        $('#alojamento').css("visibility", "hidden");
        $('#itenerario').css("visibility", "visible");
        $('#condicoes').css("visibility", "hidden");
    }
    if (id === 'condicoes') {
        $('#alojamento').css("visibility", "hidden");
        $('#itenerario').css("visibility", "hidden");
        $('#condicoes').css("visibility", "visible");
    }
}

function processErrorINFO(xhr, status, errorThrown) {
    alert("Error loading json: " + xhr.status + " " +
    xhr.statusText);
}

/*INICIAR MENU AS NOSSAS EXPERIENCIAS*/
function initMenus() {
    $(".dropdown-menu > li > a.trigger").on("click", function (e) {
        var current = $(this).next();
        var grandparent = $(this).parent().parent();
        if ($(this).hasClass('left-caret') || $(this).hasClass('right-caret'))
            $(this).toggleClass('right-caret left-caret');
        grandparent.find('.left-caret').not(this).toggleClass('right-caret left-caret');
        grandparent.find(".sub-menu:visible").not(current).hide();
        current.toggle();
        e.stopPropagation();
    });
    $(".dropdown-menu > li > a:not(.trigger)").on("click", function () {
        var root = $(this).closest('.dropdown');
        root.find('.left-caret').toggleClass('right-caret left-caret');
        root.find('.sub-menu:visible').hide();
    });
}

function enter() {
    $("#pass").keypress(function (e) {
        var key;
        if (key == 13) {
            $("#btnLogin").click();
            return false;
        }
    });
}