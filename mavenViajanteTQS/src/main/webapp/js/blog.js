﻿function initBlogs() {
    /*BLOG*/
    geralBlog();
}

var local;

function geralBlog() {
    
    $("#americaViajarSozinho").click(function () {
         $("#maincontainer").load("templates/blogslist/viajarsozinhoamerica.html")
    });


    $("#europaViajarSozinho").click(function () {
        $("#maincontainer").load("templates/blogslist/viajarsozinhoeuropa.html")
    });


    $("#africaViajarSozinho").click(function () {
        $("#maincontainer").load("templates/blogslist/viajarsozinhoafrica.html")
    });
    
    $("#oceaniaViajarSozinho").click(function () {
        $("#maincontainer").load("templates/blogslist/viajarsozinhooceania.html")
    });

    $("#americaViajarEmGrupo").click(function () {
        $("#maincontainer").load("templates/blogslist/viajarGrupoAmerica.html");
    });
    $("#europaViajarEmGrupo").click(function () {
        $("#maincontainer").load("templates/blogslist/viajarGrupoEuropa.html");
    });
    $("#africaViajarEmGrupo").click(function () {
        $("#maincontainer").load("templates/blogslist/viajarGrupoAfrica.html");
    });
    $("#oceaniaViajarEmGrupo").click(function () {
        $("#maincontainer").load("templates/blogslist/viajarGrupoOceania.html");
    });
}

function lermaisviajarsozinhoamerica(loc) {
    local = loc;
    $("#maincontainer").load("templates/blogslist/viajarsozinhoamerica" + local + ".html");
    LoadDataJsonBlog();
}

function lermaisviajargrupo(loc) {
    local = loc;
    $("#maincontainer").load("templates/blogslist/viajarGrupo.html", LoadDataJsonBlog);
}

function lermaisviajarsozinhoeuropa(loc) {
    local = loc;
    $("#maincontainer").load("templates/blogslist/viajarsozinhoeuropa" + local +".html",LoadDataJsonBlog);
}


function lermaisviajarsozinhoafrica(loc) {
    local = loc;
    $("#maincontainer").load("templates/blogslist/viajarsozinhoafrica" + local + ".html", LoadDataJsonBlog);
}


function lermaisviajarsozinhooceania(loc) {
    local = loc;
    $("#maincontainer").load("templates/blogslist/viajarsozinhooceania" + local + ".html", LoadDataJsonBlog);
}



function LoadDataJsonBlog() {
    $.ajax({
        url: "data/blog.json",
        type: "GET",
        dataType: "json",
        success: processDataJsonBlog,
        error: processDataJsonError
    });
}

function processDataJsonBlog(result, status, xhr) {
    /*VIAJAR SOZINHO*/
    /*AFRICA*/
    if (local === 'uganda') {
        var ugandaTitle = result.viajarsozinho.africa.uganda.titulo; $("#title").html(ugandaTitle);
        var ugandaAut = result.viajarsozinho.africa.uganda.autor; $("#author").html(ugandaAut);
        var ugandaDate = result.viajarsozinho.africa.uganda.data; $("#datePost").html(ugandaDate);
        var ugandaText = result.viajarsozinho.africa.uganda.texto; $("#viajarSozinhoAfricaUganda").html(ugandaText);
    }
    if (local === 'marrocos') {
        var marrocosTitle = result.viajarsozinho.africa.marrocos.titulo; $("#title").html(marrocosTitle);
        var marrocosAut = result.viajarsozinho.africa.marrocos.autor; $("#author").html(marrocosAut);
        var marrocosDate = result.viajarsozinho.africa.marrocos.data; $("#datePost").html(marrocosDate);
        var marrocosText = result.viajarsozinho.africa.marrocos.texto; $("#viajarSozinhoAfricaMarrocos").html(marrocosText);
    }
    if (local === 'cape') {
        var caboTitle = result.viajarsozinho.africa.cidadecabo.titulo; $("#title").html(caboTitle);
        var caboAut = result.viajarsozinho.africa.cidadecabo.autor; $("#author").html(caboAut);
        var caboDate = result.viajarsozinho.africa.cidadecabo.data; $("#datePost").html(caboDate);
        var caboText = result.viajarsozinho.africa.cidadecabo.texto; $("#viajarSozinhoAfricaCosta").html(caboText);
    }

    /*AMERICA*/
    if(local === 'panama'){
        var panamaTitle = result.viajarsozinho.america.panama.titulo; $("#title").html(panamaTitle);
        var panamaAut = result.viajarsozinho.america.panama.autor; $("#author").html(panamaAut);
        var panamaDate = result.viajarsozinho.america.panama.data; $("#datePost").html(panamaDate);
        var panamaText = result.viajarsozinho.america.panama.texto; $("#viajarSozinhoAmericaPanama").html(panamaText);
    } 
    if(local === 'costarica'){
        var costaricaTitle = result.viajarsozinho.america.costarica.titulo; $("#title").html(costaricaTitle);
        var costaricaAut = result.viajarsozinho.america.costarica.autor; $("#author").html(costaricaAut);
        var costaricaDate = result.viajarsozinho.america.costarica.data; $("#datePost").html(costaricaDate);
        var costaricaText = result.viajarsozinho.america.costarica.texto; $("#viajarSozinhoAmericaCostaRica").html(costaricaText);
    }
    if (local === 'sanblas') {
        var sanablasTitle = result.viajarsozinho.america.sanblas.titulo; $("#title").html(sanablasTitle);
        var sanablasAut = result.viajarsozinho.america.sanblas.autor; $("#author").html(sanablasAut);
        var sanablasDate = result.viajarsozinho.america.sanblas.data; $("#datePost").html(sanablasDate);
        var sanablasText = result.viajarsozinho.america.sanblas.texto; $("#viajarSozinhoAmericaSanblas").html(sanablasText);
    }

    /*EUROPA*/
    if (local === 'douro') {
        var douroTitle = result.viajarsozinho.europa.douro.titulo; $("#title").html(douroTitle);
        var douroAut = result.viajarsozinho.europa.douro.autor; $("#author").html(douroAut);
        var douroDate = result.viajarsozinho.europa.douro.data; $("#datePost").html(douroDate);
        var douroText = result.viajarsozinho.europa.douro.texto; $("#viajarSozinhoEuropaDouro").html(douroText);
    }
    if (local === 'copenhaga') {
        var copenhagaTitle = result.viajarsozinho.europa.copenhaga.titulo; $("#title").html(copenhagaTitle);
        var copenhagaAut = result.viajarsozinho.europa.copenhaga.autor; $("#author").html(copenhagaAut);
        var copenhagaDate = result.viajarsozinho.europa.copenhaga.data; $("#datePost").html(copenhagaDate);
        var copenhagaText = result.viajarsozinho.europa.copenhaga.texto; $("#viajarSozinhoEuropaCopenhaga").html(copenhagaText);
    }
    if (local === 'suecia') {
        var sueciaTitle = result.viajarsozinho.europa.suecia.titulo; $("#title").html(sueciaTitle);
        var sueciaAut = result.viajarsozinho.europa.suecia.autor; $("#author").html(sueciaAut);
        var sueciaDate = result.viajarsozinho.europa.suecia.data; $("#datePost").html(sueciaDate);
        var sueciaText = result.viajarsozinho.europa.suecia.texto; $("#viajarSozinhoEuropaSuecia").html(sueciaText);
    }

    /*OCEANIA*/
    if (local === 'sidney') {
        var sidneyTitle = result.viajarsozinho.oceania.sidney.titulo; $("#title").html(sidneyTitle);
        var sidneyAut = result.viajarsozinho.oceania.sidney.autor; $("#author").html(sidneyAut);
        var sidneyDate = result.viajarsozinho.oceania.sidney.data; $("#datePost").html(sidneyDate);
        var sidneyText = result.viajarsozinho.oceania.sidney.texto; $("#viajarSozinhoOceaniaSidney").html(sidneyText);
    }
    if (local === 'novazelandia') {
        var zelandiaTitle = result.viajarsozinho.oceania.novazelandia.titulo; $("#title").html(zelandiaTitle);
        var zelandiaAut = result.viajarsozinho.oceania.novazelandia.autor; $("#author").html(zelandiaAut);
        var zelandiaDate = result.viajarsozinho.oceania.novazelandia.data; $("#datePost").html(zelandiaDate);
        var zelandiaText = result.viajarsozinho.oceania.novazelandia.texto; $("#viajarSozinhoOceaniaNovaZelandia").html(zelandiaText);
    }
    if (local === 'fiji') {
        var fijiTitle = result.viajarsozinho.oceania.fiji.titulo; $("#title").html(fijiTitle);
        var fijiAut = result.viajarsozinho.oceania.fiji.autor; $("#author").html(fijiAut);
        var fijiDate = result.viajarsozinho.oceania.fiji.data; $("#datePost").html(fijiDate);
        var fijiText = result.viajarsozinho.oceania.fiji.texto; $("#viajarSozinhoOceaniaFiji").html(fijiText);
    }

    /*VIAJAR GRUPO*/
    /*AMERICA*/
    if (local === 'cancun') {
        //alert(result.viajarGrupo.america.cancun.foto);
        $("#foto").attr("src", "../../img/blog/imggrupo/" + result.viajarGrupo.america.cancun.foto);
        var title = result.viajarGrupo.america.cancun.titulo; $("#title").html(title);
        var author = result.viajarGrupo.america.cancun.autor; $("#author").html(author);
        var date = result.viajarGrupo.america.cancun.data; $("#datePost").html(date);
        var text = result.viajarGrupo.america.cancun.texto; $("#viajargrupo").html(text);
    }
    if (local === 'riojaneiro') {
        $("#foto").attr("src", "../../img/blog/imggrupo/" + result.viajarGrupo.america.riojaneiro.foto);
        var title = result.viajarGrupo.america.riojaneiro.titulo; $("#title").html(title);
        var author = result.viajarGrupo.america.riojaneiro.autor; $("#author").html(author);
        var date = result.viajarGrupo.america.riojaneiro.data; $("#datePost").html(date);
        var text = result.viajarGrupo.america.riojaneiro.texto; $("#viajargrupo").html(text);

    }
    if (local === 'bahamas') {
        $("#foto").attr("src", "../../img/blog/imggrupo/" + result.viajarGrupo.america.bahamas.foto);
        var title = result.viajarGrupo.america.bahamas.titulo; $("#title").html(title);
        var author = result.viajarGrupo.america.bahamas.autor; $("#author").html(author);
        var date = result.viajarGrupo.america.bahamas.data; $("#datePost").html(date);
        var text = result.viajarGrupo.america.bahamas.texto; $("#viajargrupo").html(text);

    }
    /*AFRICA*/
    if (local === 'quenia') {
        $("#foto").attr("src", "../../img/blog/imggrupo/" + result.viajarGrupo.africa.quenia.foto);
        var title = result.viajarGrupo.africa.quenia.titulo; $("#title").html(title);
        var author = result.viajarGrupo.africa.quenia.autor; $("#author").html(author);
        var date = result.viajarGrupo.africa.quenia.data; $("#datePost").html(date);
        var text = result.viajarGrupo.africa.quenia.texto; $("#viajargrupo").html(text);
    }
    if (local === 'tunisia') {
        $("#foto").attr("src", "../../img/blog/imggrupo/" + result.viajarGrupo.africa.tunisia.foto);
        var title = result.viajarGrupo.africa.tunisia.titulo; $("#title").html(title);
        var author = result.viajarGrupo.africa.tunisia.autor; $("#author").html(author);
        var date = result.viajarGrupo.africa.tunisia.data; $("#datePost").html(date);
        var text = result.viajarGrupo.africa.tunisia.texto; $("#viajargrupo").html(text);
    }
    if (local === 'luanda') {
        $("#foto").attr("src", "../../img/blog/imggrupo/" + result.viajarGrupo.africa.luanda.foto);
        var title = result.viajarGrupo.africa.luanda.titulo; $("#title").html(title);
        var author = result.viajarGrupo.africa.luanda.autor; $("#author").html(author);
        var date = result.viajarGrupo.africa.luanda.data; $("#datePost").html(date);
        var text = result.viajarGrupo.africa.luanda.texto; $("#viajargrupo").html(text);
    }
    /*EUROPA*/
    if (local === 'londres') {
        $("#foto").attr("src", "../../img/blog/imggrupo/" + result.viajarGrupo.europa.londres.foto);
        var title = result.viajarGrupo.europa.londres.titulo; $("#title").html(title);
        var author = result.viajarGrupo.europa.londres.autor; $("#author").html(author);
        var date = result.viajarGrupo.europa.londres.data; $("#datePost").html(date);
        var text = result.viajarGrupo.europa.londres.texto; $("#viajargrupo").html(text);
    }
    if (local === 'irlanda') {
        $("#foto").attr("src", "../../img/blog/imggrupo/" + result.viajarGrupo.europa.irlanda.foto);
        var title = result.viajarGrupo.europa.irlanda.titulo; $("#title").html(title);
        var author = result.viajarGrupo.europa.irlanda.autor; $("#author").html(author);
        var date = result.viajarGrupo.europa.irlanda.data; $("#datePost").html(date);
        var text = result.viajarGrupo.europa.irlanda.texto; $("#viajargrupo").html(text);
    }
    if (local === 'paris') {
        $("#foto").attr("src", "../../img/blog/imggrupo/" + result.viajarGrupo.europa.paris.foto);
        var title = result.viajarGrupo.europa.paris.titulo; $("#title").html(title);
        var author = result.viajarGrupo.europa.paris.autor; $("#author").html(author);
        var date = result.viajarGrupo.europa.paris.data; $("#datePost").html(date);
        var text = result.viajarGrupo.europa.paris.texto; $("#viajargrupo").html(text);
    }
    /*OCEANIA*/
    if (local === 'melbourne') {
        $("#foto").attr("src", "../../img/blog/imggrupo/" + result.viajarGrupo.oceania.melbourne.foto);
        var title = result.viajarGrupo.oceania.melbourne.titulo; $("#title").html(title);
        var author = result.viajarGrupo.oceania.melbourne.autor; $("#author").html(author);
        var date = result.viajarGrupo.oceania.melbourne.data; $("#datePost").html(date);
        var text = result.viajarGrupo.oceania.melbourne.texto; $("#viajargrupo").html(text);
    }
    if (local === 'nz') {
        $("#foto").attr("src", "../../img/blog/imggrupo/" + result.viajarGrupo.oceania.nz.foto);
        var title = result.viajarGrupo.oceania.nz.titulo; $("#title").html(title);
        var author = result.viajarGrupo.oceania.nz.autor; $("#author").html(author);
        var date = result.viajarGrupo.oceania.nz.data; $("#datePost").html(date);
        var text = result.viajarGrupo.oceania.nz.texto; $("#viajargrupo").html(text);
    }
    if (local === 'sidney') {
        $("#foto").attr("src", "../../img/blog/imggrupo/" + result.viajarGrupo.oceania.sidney.foto);
        var title = result.viajarGrupo.oceania.sidney.titulo; $("#title").html(title);
        var author = result.viajarGrupo.oceania.sidney.autor; $("#author").html(author);
        var date = result.viajarGrupo.oceania.sidney.data; $("#datePost").html(date);
        var text = result.viajarGrupo.oceania.sidney.texto; $("#viajargrupo").html(text);
    }
}

function processDataJsonError(xhr, status, errorThrown) {
    alert("Error loading json: " + xhr.status + " " +
    xhr.statusText);
}