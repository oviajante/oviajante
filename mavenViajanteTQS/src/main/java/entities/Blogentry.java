/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author tiagoteixeira
 */
@Entity
@Table(name = "BLOGENTRY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Blogentry.findAll", query = "SELECT b FROM Blogentry b"),
    @NamedQuery(name = "Blogentry.findById", query = "SELECT b FROM Blogentry b WHERE b.id = :id"),
    @NamedQuery(name = "Blogentry.findByData", query = "SELECT b FROM Blogentry b WHERE b.data = :data"),
    @NamedQuery(name = "Blogentry.findByImagemurl", query = "SELECT b FROM Blogentry b WHERE b.imagemurl = :imagemurl"),
    @NamedQuery(name = "Blogentry.findByLocal", query = "SELECT b FROM Blogentry b WHERE b.local = :local"),
    @NamedQuery(name = "Blogentry.findByTexto", query = "SELECT b FROM Blogentry b WHERE b.texto = :texto"),
    @NamedQuery(name = "Blogentry.findByTipoviagem", query = "SELECT b FROM Blogentry b WHERE b.tipoviagem = :tipoviagem"),
    @NamedQuery(name = "Blogentry.findByTitulo", query = "SELECT b FROM Blogentry b WHERE b.titulo = :titulo")})
public class Blogentry implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @GeneratedValue
    @Column(name = "ID")
    private Long id;
    @Size(max = 255)
    @Column(name = "DATA")
    private String data;
    @Size(max = 255)
    @Column(name = "IMAGEMURL")
    private String imagemurl;
    @Size(max = 255)
    @Column(name = "LOCAL")
    private String local;
    @Size(max = 255)
    @Column(name = "TEXTO")
    private String texto;
    @Size(max = 255)
    @Column(name = "TIPOVIAGEM")
    private String tipoviagem;
    @Size(max = 255)
    @Column(name = "TITULO")
    private String titulo;
    @JoinColumn(name = "ID_UTILIZADOR_ID", referencedColumnName = "ID")
    @ManyToOne
    private Utilizador idUtilizadorId;

    public Blogentry() {
    }

    public Blogentry(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getImagemurl() {
        return imagemurl;
    }

    public void setImagemurl(String imagemurl) {
        this.imagemurl = imagemurl;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getTipoviagem() {
        return tipoviagem;
    }

    public void setTipoviagem(String tipoviagem) {
        this.tipoviagem = tipoviagem;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Utilizador getIdUtilizadorId() {
        return idUtilizadorId;
    }

    public void setIdUtilizadorId(Utilizador idUtilizadorId) {
        this.idUtilizadorId = idUtilizadorId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Blogentry)) {
            return false;
        }
        Blogentry other = (Blogentry) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Blogentry[ id=" + id + " ]";
    }
    
}
