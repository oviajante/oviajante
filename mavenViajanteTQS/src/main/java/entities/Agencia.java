/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author tiagoteixeira
 */
@Entity
@Table(name = "AGENCIA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Agencia.findAll", query = "SELECT a FROM Agencia a"),
    @NamedQuery(name = "Agencia.findById", query = "SELECT a FROM Agencia a WHERE a.id = :id"),
    @NamedQuery(name = "Agencia.findByCompanhia", query = "SELECT a FROM Agencia a WHERE a.companhia = :companhia"),
    @NamedQuery(name = "Agencia.findByPassword", query = "SELECT a FROM Agencia a WHERE a.password = :password"),
    @NamedQuery(name = "Agencia.findByUtilizador", query = "SELECT a FROM Agencia a WHERE a.utilizador = :utilizador")})
public class Agencia implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Size(max = 255)
    @Column(name = "COMPANHIA")
    private String companhia;
    @Size(max = 255)
    @Column(name = "PASSWORD")
    private String password;
    @Size(max = 255)
    @Column(name = "UTILIZADOR")
    private String utilizador;
    @OneToMany(mappedBy = "idAgenciaId")
    private Collection<Pacoteviagem> pacoteviagemCollection;

    public Agencia() {
    }

    public Agencia(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanhia() {
        return companhia;
    }

    public void setCompanhia(String companhia) {
        this.companhia = companhia;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUtilizador() {
        return utilizador;
    }

    public void setUtilizador(String utilizador) {
        this.utilizador = utilizador;
    }

    @XmlTransient
    public Collection<Pacoteviagem> getPacoteviagemCollection() {
        return pacoteviagemCollection;
    }

    public void setPacoteviagemCollection(Collection<Pacoteviagem> pacoteviagemCollection) {
        this.pacoteviagemCollection = pacoteviagemCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Agencia)) {
            return false;
        }
        Agencia other = (Agencia) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Agencia[ id=" + id + " ]";
    }

}
