/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.presentation;

import boundary.ReservaFacade;
import entities.Reserva;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author sarafurao
 */
@Named(value = "ReservaView")
@RequestScoped
public class ReservaView {

    @EJB
    private ReservaFacade reservaFacade;

    Reserva reserva;

    public ReservaView(Reserva reserva) {
        this.reserva = reserva;
    }

}
