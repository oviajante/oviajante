/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.presentation;

import boundary.BlogentryFacade;
import boundary.UtilizadorFacade;
import entities.Blogentry;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author sarafurao
 */
//@Named(value = "BlogentryView")
@ManagedBean(name = "BlogentryView")
@RequestScoped
public class BlogentryView {

    @EJB
    private BlogentryFacade blogentryFacade;

    Blogentry blogentry;

    public BlogentryView() {
    }

    public BlogentryView(Blogentry blogentry) {
        this.blogentry = blogentry;
    }

    public List<Blogentry> getBlogEntries() {
        return blogentryFacade.findAll();
    }


}
