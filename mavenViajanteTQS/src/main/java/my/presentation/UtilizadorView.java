/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.presentation;

import boundary.UtilizadorFacade;
import entities.Utilizador;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author sarafurao
 */
@Named(value = "UtilizadorView")
@RequestScoped
public class UtilizadorView {

    @EJB
    private UtilizadorFacade utilizadorFacade;

    Utilizador utilizador;

    public UtilizadorView(Utilizador utilizador) {
        this.utilizador = utilizador;
    }

}
