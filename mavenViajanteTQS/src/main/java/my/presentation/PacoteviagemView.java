/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.presentation;

import boundary.PacoteviagemFacade;
import entities.Pacoteviagem;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author sarafurao
 */
@Named(value = "PacoteviagemView")
@RequestScoped
public class PacoteviagemView {

    @EJB
    private PacoteviagemFacade pacoteviagemFacade;
    String tipoPacote = new String();
    String selectedPacote = new String();

    Pacoteviagem pacoteviagem;
    

    public PacoteviagemView() {
    }

    public PacoteviagemView(Pacoteviagem pacoteviagem) {
        this.pacoteviagem = pacoteviagem;
    }

    public String getTipoPacote() {
        FacesContext fc = FacesContext.getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        tipoPacote = params.get("tipopacote");
        return tipoPacote;
    }

    public List<Pacoteviagem> getPacotes(String tipoPacote) {
        if (tipoPacote.equals("Cruzeiros")) {
            tipoPacote = "cruzeiros";
        } else if (tipoPacote.equals("Disneyland")) {
            tipoPacote = "disney";
        } else if (tipoPacote.equals("Praias e Ilhas")) {
            tipoPacote = "praias";
        } else if (tipoPacote.equals("Escapadinhas")) {

        } else if (tipoPacote.equals("Grandes Viagens")) {

        } else {
            tipoPacote = "promocoes";
        }
        return pacoteviagemFacade.getPacotesTipo(tipoPacote);
    }

    public String getSelectedPacote() {
        FacesContext fc = FacesContext.getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        selectedPacote = params.get("selectedPacote");
        return selectedPacote;
    }
    
    public Pacoteviagem getDetailPacote(String id) {
        return pacoteviagemFacade.find(new Long(Integer.parseInt(id)));
    }

}
