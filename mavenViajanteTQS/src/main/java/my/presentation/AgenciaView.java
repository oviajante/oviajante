/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.presentation;

import boundary.AgenciaFacade;
import entities.Agencia;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author sarafurao
 */
//@Named(value = "AgenciaView")
@ManagedBean(name = "AgenciaView")
@RequestScoped
public class AgenciaView {

    @EJB
    private AgenciaFacade agenciaFacade;

    private Agencia agencia;

    public AgenciaView() {
    }

    public AgenciaView(Agencia agencia) {
        this.agencia = agencia;
    }

    public Agencia getMessage() {
        return agencia;
    }

    public int getNumberOfAgencias() {
        return agenciaFacade.findAll().size();
    }

    public void addAgencia() {
        this.agenciaFacade.persist(agencia);
    }
}
