package entities;

import entities.Agencia;
import entities.Reserva;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-06-09T19:00:30")
@StaticMetamodel(Pacoteviagem.class)
public class Pacoteviagem_ { 

    public static volatile SingularAttribute<Pacoteviagem, Long> id;
    public static volatile SingularAttribute<Pacoteviagem, String> alojamento;
    public static volatile SingularAttribute<Pacoteviagem, String> preco;
    public static volatile SingularAttribute<Pacoteviagem, String> titulo;
    public static volatile SingularAttribute<Pacoteviagem, String> condicoes;
    public static volatile SingularAttribute<Pacoteviagem, String> tipopacote;
    public static volatile SingularAttribute<Pacoteviagem, String> detalhe;
    public static volatile SingularAttribute<Pacoteviagem, String> imagemurl;
    public static volatile CollectionAttribute<Pacoteviagem, Reserva> reservaCollection;
    public static volatile SingularAttribute<Pacoteviagem, Agencia> idAgenciaId;
    public static volatile SingularAttribute<Pacoteviagem, String> itenerario;

}