package entities;

import entities.Blogentry;
import entities.Reserva;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-06-09T19:00:30")
@StaticMetamodel(Utilizador.class)
public class Utilizador_ { 

    public static volatile SingularAttribute<Utilizador, Long> id;
    public static volatile SingularAttribute<Utilizador, Integer> telemovel;
    public static volatile CollectionAttribute<Utilizador, Blogentry> blogentryCollection;
    public static volatile SingularAttribute<Utilizador, String> email;
    public static volatile SingularAttribute<Utilizador, String> data;
    public static volatile SingularAttribute<Utilizador, String> nome;
    public static volatile SingularAttribute<Utilizador, String> utilizador;
    public static volatile CollectionAttribute<Utilizador, Reserva> reservaCollection;
    public static volatile SingularAttribute<Utilizador, String> password;

}